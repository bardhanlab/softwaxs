#!/usr/bin/perl

if ($#ARGV < 1) {
	 print "Usage:\n\tpdb_cry.pl input.pdb output.cry\n";
	 exit(0);
}

$inPdb = $ARGV[0];
$outCry = $ARGV[1];

open(INFILE, $inPdb) || die "Could not open $inPdb ... dying.\n";
open(OUTFILE,">$outCry") || die "Could not open $outCry ... dying.\n";
@lines = <INFILE>;
$numAtoms = grep /^ATOM/, @lines;
$numHetatms = grep /^HETATM/, @lines;
$numtotal = $numAtoms + $numHetatms;
print OUTFILE " $numtotal\n";
foreach (@lines) {
	 @words = getPDBline($_);
	 if ($words[0] =~ /^ATOM/ || $words[0] =~ /^HETATM/) {
		  $atomName = $words[2];
		  $resName = $words[3];
		  $element = matchElement($atomName, $resname);
		  if ($element eq "UNK") {
				$default = substr($atomName, 0, 1);
				if ($default =~ /[0-9]/) {
				    $default = substr($atomName, 1, 1);
				}
				print "WARNING: atom number $words[1], name $words[2], residue $words[3] was not recognized.  ";
				print "assigning it $default.\n";
				$element = $default;
		  }
		  $newline = sprintf "%-4.4s  %10.5f%10.5f%10.5f%10.5f\n", $element, $words[4], $words[5], $words[6], 0.00;
		  print OUTFILE $newline;
		  
	 } else {  
		  next; # note that this will not copy other lines!!
	 }	 
		  
}
close(OUTFILE);
close(INFILE);

sub getPDBline {
	 $_ = shift(@_);
	 $len = length($_);
	 @words = ();
	 $words[0] = trim(substr($_, 0, 6));
	 if ($words[0] !~ /ATOM/ && $words[0] !~ /HETATM/ ) {
	     return @words;
	 }
	 $words[1] = trim(substr($_,6,5));
	 $words[2] = trim(substr($_,12,4));
	 $words[3] = trim(substr($_,17,3));
	 $words[4] = trim(substr($_,30,8));
	 $words[5] = trim(substr($_,38,8));
	 if ($len <= 54) {
		  $words[6] = trim(substr($_,46));
	 }
	 else {
		  $words[6] = trim(substr($_,46,8));
	 }
		  
	 return @words;
}

sub matchElement {
	 my ($atomName, $resName) = @_;
	 $element = "UNK";
	 if ($atomName eq "N" || $atomName =~ /ND/ || $atomName =~ /NE/ || $atomName =~ /NH/ || $atomName =~ /NZ/ ) {
		  $element = "N";
	 } elsif ($atomName eq "O" || $atomName =~ /OE/ || $atomName =~ /OD/ || $atomName =~ /OH/ || $atomName =~ /OG/ || $atomName =~ /OXT/) {
		  $element = "O";
	 } elsif ($atomName eq "C" || $atomName eq "CA" || $atomName =~ /CB/ || $atomName =~ /CD/ || $atomName =~ /CE/ || $atomName =~ /CG/ || $atomName =~ /CZ/ || $atomName =~ /CH/ ) {
		  $element = "C";
	 } elsif ($atomName eq "SD") {
		  $element = "S";
	 } elsif ($atomName =~ /^H/) {
		  $element = "H";
	 } elsif ($atomName eq "SOD") {  # NA is something in hemoglobin
		  $element = "NA";
	 } elsif ($atomName eq "FE") {
		  $element = "FE";
	 } elsif ($atomName eq "CLA") {
		  $element = "CL";
	 } elsif ($atomName eq "CAL") {
		  $element = "CA";
	 }

	 
	 return $element;
}

sub trim($)
{
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}

#ifndef __SCATTERINTENSITIES_H__
#define __SCATTERINTENSITIES_H__

typedef struct _ScatterIntensities {
  real minQ, maxQ;
  unsigned int numQpoints;
  real *sAngInv;
  real *IsAngInv;
  real *IsFMSAngInv;
  real *DebyeAngInv;
  real *ExclAngInv;
  real *BorderAngInv;
  real *FMSExcl;
  real *testIsAngInv;

  real *IsDebye;
  real *IsDebyePlusHS;
  real *IsDebyePlusGaussian;
  real *IsHardSphere;
  real *IsGaussian;
} _ScatterIntensities; 
typedef struct _ScatterIntensities* ScatterIntensities;

ScatterIntensities ScatterIntensities_allocate(ScatterOptions options, PBEproblem problem);
void ScatterIntensities_free(ScatterIntensities intensities);
void ScatterIntensities_output(char *filename, ScatterIntensities intensities);
#endif

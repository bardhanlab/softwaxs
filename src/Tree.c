#include "FFTSVD.h"
#ifdef OMP
#include <omp.h>
#endif

/* Constructors and Destructors */

Tree Tree_allocate(unsigned int resolution, Panel* panels, unsigned int numpanels, Vector3D* points, unsigned int numpoints, unsigned int maxpanelsperfinestcube, BEMKernelType kerneltype, void* parameters, unsigned int gridpoints, real epsilon, BEMLayerType layertype, real minimumcubesize) {
   Tree tree = (Tree)calloc(1, sizeof(_Tree));
   unsigned int* panelindices = (unsigned int*)calloc(numpanels, sizeof(unsigned int));
   unsigned int* pointindices = (unsigned int*)calloc(numpoints, sizeof(unsigned int));
   unsigned int indices[3] = {0, 0, 0};
   Vector3D bounds[2];
   unsigned int p;
   int i;
   real minx = 1000000.0, miny = 1000000.0, minz = 1000000.0;
   real maxx = -1000000.0, maxy = -1000000.0, maxz = -1000000.0;
   real centerx, centery, centerz, maximumextent = 0.0;
   
   tree->panels = panels;
   tree->numpanels = numpanels;
   tree->points = points;
   tree->numpoints = numpoints;
   tree->maxpanelsperfinestcube = maxpanelsperfinestcube;
   tree->kerneltype = kerneltype;
   tree->parameters = parameters;
   tree->gridpoints = gridpoints;
   tree->epsilon = epsilon;
   tree->layertype = layertype;
   tree->minimumcubesize = minimumcubesize;

	if (tree->layertype == NORMDERIV_SINGLE_LAYER_INT)
	  	tree->normals = &(points[numpoints]);  // only used if we have NORMDERIV_SINGLE_LAYER_INT used. jpb 9/21/08

   /* Initialize cube hierarchy */

   for (p = 0; p < numpanels; p++)
      panelindices[p] = p;

   for (p = 0; p < numpoints; p++)
      pointindices[p] = p;

   bounds[0] = Vector3D_allocate();
   bounds[1] = Vector3D_allocate();

   for (p = 0; p < numpanels; p++) {
      if (panels[p]->centroid->x < minx) minx = panels[p]->centroid->x;
      if (panels[p]->centroid->y < miny) miny = panels[p]->centroid->y;
      if (panels[p]->centroid->z < minz) minz = panels[p]->centroid->z;
      if (panels[p]->centroid->x > maxx) maxx = panels[p]->centroid->x;
      if (panels[p]->centroid->y > maxy) maxy = panels[p]->centroid->y;
      if (panels[p]->centroid->z > maxz) maxz = panels[p]->centroid->z;
   }

   for (p = 0; p < numpoints; p++) {
      if (points[p]->x < minx) minx = points[p]->x;
      if (points[p]->y < miny) miny = points[p]->y;
      if (points[p]->z < minz) minz = points[p]->z;
      if (points[p]->x > maxx) maxx = points[p]->x;
      if (points[p]->y > maxy) maxy = points[p]->y;
      if (points[p]->z > maxz) maxz = points[p]->z;
   }

   /* This gives rectangular cubes */
   /*
     bounds[0]->x = minx;
     bounds[0]->y = miny;
     bounds[0]->z = minz;
     bounds[1]->x = maxx;
     bounds[1]->y = maxy;
     bounds[1]->z = maxz;
   */
    
   /* This gives cubic cubes */

   centerx = (minx + maxx) * 0.5;
   centery = (miny + maxy) * 0.5;
   centerz = (minz + maxz) * 0.5;
   
   if ((maxx - minx) > maximumextent)
      maximumextent = maxx - minx;
   if ((maxy - miny) > maximumextent)
      maximumextent = maxy - miny;
   if ((maxz - minz) > maximumextent)
      maximumextent = maxz - minz;

   bounds[0]->x = centerx - maximumextent * 0.5;
   bounds[0]->y = centery - maximumextent * 0.5;
   bounds[0]->z = centerz - maximumextent * 0.5;
   bounds[1]->x = centerx + maximumextent * 0.5;
   bounds[1]->y = centery + maximumextent * 0.5;
   bounds[1]->z = centerz + maximumextent * 0.5;

	Cube_free(tree->root);
	tree->partitioningdepth = resolution;
	tree->root = Cube_allocate(panels, panelindices, numpanels, points, pointindices, numpoints, 0, indices, bounds, NULL, tree, tree->partitioningdepth);
   printf("Using %u partitioning levels\n", tree->partitioningdepth);

   /* Initialize gridpointsperlevel */

   tree->gridpointsperlevel = (unsigned int*)calloc(tree->partitioningdepth, sizeof(unsigned int));

   /* Right now, set every level to the user specified value */
/*
     for (i = 0; i < tree->partitioningdepth; i++)
        tree->gridpointsperlevel[i] = tree->gridpoints;
*/
   /* Or, use what maltman found is reasonable.  Use the user-specified 
      grid size for the finest two levels, and increase one grid size for
      each level coarser than that.  This should cause no noticable 
      performance or memory difference, but increase accuracy significantly. */

   tree->gridpointsperlevel[tree->partitioningdepth-1] = tree->gridpoints;

   if (tree->partitioningdepth >= 2)
      tree->gridpointsperlevel[tree->partitioningdepth-2] = tree->gridpoints;

   if (tree->partitioningdepth >= 3) {
      p = 1;

      for (i = tree->partitioningdepth-3; i >= 0; i--) {
         tree->gridpointsperlevel[i] = tree->gridpoints + p;
         p++;
      }
   }

   free(panelindices);
   free(pointindices);
   Vector3D_free(bounds[0]);
   Vector3D_free(bounds[1]);
   
   return tree;
}

void Tree_free(Tree tree) {
   unsigned int dimension = 3 + 4 * LOCAL;
   unsigned int i, j, k, d;

   Cube_free(tree->root);

   free(tree->gridpointsperlevel);

   free(tree);
}

/* Operations */

void Tree_lists(Tree tree) {
   Cube_lists(tree->root);
}

void Tree_memory(Tree tree) {
   unsigned int padgridsize = (2*tree->gridpoints-1)*(2*tree->gridpoints-1)*((2*tree->gridpoints-1)/2+1);
   unsigned int Tmem = tree->partitioningdepth *
      ((3 + 4 * LOCAL) * (3 + 4 * LOCAL) * (3 + 4 * LOCAL) -
       (1 + 2 * LOCAL) * (1 + 2 * LOCAL) * (1 + 2 * LOCAL)) *
      padgridsize * 
      sizeof(complexreal);

   unsigned int Cubemem = 0, Umem = 0, VTmem = 0, UTImem = 0, PVmem = 0, Kmem = 0, Dmem = 0;
   unsigned int Panelmem = 0, i;

   for (i = 0; i < tree->numpanels; i++)
      Panelmem += Panel_memory(tree->panels[i], tree->layertype);

   Cube_memory(tree->root, &Cubemem, &Umem, &VTmem, &UTImem, &PVmem, &Kmem, &Dmem);

   printf("Memory Use For Panels: %u\n", Panelmem);
   printf("Memory Use For Cubes: %u\n", Cubemem);
   printf("Memory Use For U: %u\n", Umem);
   printf("Memory Use For VT: %u\n", VTmem);
   printf("Memory Use For UTI: %u\n", UTImem);
   printf("Memory Use For PV: %u\n", PVmem);
   printf("Memory Use For T: %u\n", Tmem);
   printf("Memory Use For D: %u\n", Dmem);
   printf("Memory Use Total: %u\n", Panelmem+Cubemem+Umem+VTmem+UTImem+PVmem+Tmem+Kmem+Dmem);
}

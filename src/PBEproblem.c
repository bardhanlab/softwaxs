#include "PBEproblem.h"
#include "Integration.h"
#include <sys/time.h>
#include <sys/resource.h>

PBEproblem PBEproblem_allocate(char *PDBfilename, char *SRFfilename, unsigned int resolution) {
  PBEproblem problem = NULL;
  problem = (PBEproblem)(calloc(1, sizeof(_PBEproblem)));
  PBEproblem_allocateButDontCompress(problem, PDBfilename, SRFfilename);
  PBEproblem_initialize(problem, resolution);
  return problem;
}

PBEproblem PBEproblem_loadOnlyPDB(char *PDBfilename) {
  PBEproblem problem = NULL;
  problem = (PBEproblem)(calloc(1, sizeof(_PBEproblem)));
  problem->pbesurfaceoperator = NULL;
  readPDB(PDBfilename, &(problem->numpdbentries), &(problem->pdbentries));

  return problem;
}

void PBEproblem_allocateButDontCompress(PBEproblem problem, char *PDBfilename, char *SRFfilename) {
  problem->pbesurfaceoperator = NULL;

   readPDB(PDBfilename, &(problem->numpdbentries), &(problem->pdbentries));
   readSRF(SRFfilename,
           &(problem->saltpanels), &(problem->numsaltpanels), &(problem->numsalts),
           &(problem->dielectricpanels), &(problem->numdielectricpanels),
           &(problem->numdielectrics),  &(problem->dielectricparent),
           &(problem->dielectriccavitypanels), &(problem->numdielectriccavitypanels),
           &(problem->numdielectriccavities), &(problem->dielectriccavityparent),
           &(problem->saltcavitypanels), &(problem->numsaltcavitypanels),
           &(problem->numsaltcavities), &(problem->saltcavityparent),
           &(problem->numtotalpanels));

   problem->numvariablecharges = 0;
   problem->numfixedligandcharges = 0;
   problem->numfixedreceptorcharges = 0;
   
   problem->globalCharges = Vector_allocate(problem->numpdbentries);

   problem->numtotalsurfacevariables = 2 * problem->numtotalpanels;

   problem->RHS = Vector_allocate(problem->numtotalsurfacevariables);
   problem->Sol = Vector_allocate(problem->numtotalsurfacevariables);

   problem->globalPhiReact = Vector_allocate(problem->numpdbentries);
   problem->variablePhiReact = Vector_allocate(problem->numvariablecharges);
}

void PBEproblem_initialize(PBEproblem problem, unsigned int resolution)
{
   printf("found %d %d %d charges\n",
          problem->numvariablecharges,
          problem->numfixedligandcharges,
          problem->numfixedreceptorcharges);

   struct rusage ruse;
   struct timeval tval;
   real starttime, endtime, startwalltime, endwalltime;

   printf("generating Green's theorem operator\n");
      
   generateSurfaceOperator(resolution, &(problem->pbesurfaceoperator), problem->pdbentries, problem->numpdbentries,
                           problem->saltpanels, problem->numsaltpanels, problem->numsalts,
                           problem->dielectricpanels, problem->numdielectricpanels,
                           problem->numdielectrics, problem->dielectricparent,
                           problem->dielectriccavitypanels, problem->numdielectriccavitypanels,
                           problem->numdielectriccavities, problem->dielectriccavityparent,
                           problem->saltcavitypanels, problem->numsaltcavitypanels,
                           problem->numsaltcavities, problem->saltcavityparent,
                           problem->numtotalpanels);
}

void PBEproblem_free(PBEproblem problem) {
  if (problem->pbesurfaceoperator)
    SurfaceOperator_free(problem->pbesurfaceoperator);
  
  // what about freeing panels, PDBentries?
   free(problem->variablechargeindextoglobalindex);

   Vector_free(problem->globalCharges);
   Vector_free(problem->RHS);
   Vector_free(problem->Sol);
   Vector_free(problem->globalPhiReact);
   Vector_free(problem->variablePhiReact);
   free(problem);
}


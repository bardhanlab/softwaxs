#include "CRYinput.h"
#include "PBEproblem.h"
#include "scatterparameters.h"

/* void getDebyeAtParticularAngle(PBEproblem problem, CRY_input cryInput, real proteinDensity, */
/* 			       real q, real *IqDebyeOnly, */
/* 			       real *IqHardSphereFMS, */
/* 			       real *IqGaussianFMS, */
/* 			       real *IqDebyePlusHS, */
/* 			       real *IqDebyePlusGaussian) { */
/*   unsigned int i, j, k; */
/*   unsigned int startIndex1, startIndex2; */
/*   real *Aterm = lowA; */
/*   real *Bterm = lowB; */
/* #ifdef MADMAX */
/*   complexreal f1, f2; */
/*   real f1prime, f2prime;  // the real part of the anomalous scattering */
/*   real f1doubleprime, f2doubleprime;  // the imaginary part of the anomalous scattering */
/* #else */
/*   real f1,f2; */
/* #endif */
/*   real r; */
/*   complexreal cIqDebyePlusHS = 0.0; */
/*   complexreal cIqHardSphereFMS = 0.0; */
/*   complexreal cIqGaussianFMS = 0.0; */
/*   complexreal cIqDebyePlusGaussian = 0.0; */
/*   complexreal cIqDebyeOnly = 0.0; */
/*   real s = q; */
/*   real sinThetaOverLambda  = q/(4.0 * M_PI); */
/*   //  printf("sinThetaOverLambda : %f\n",(float)sinThetaOverLambda); */
/*   for (i = 0; i < cryInput->numAtoms; i++) { */
/* 	 //	 printf("atomType[%d] = %d\n", i, cryInput->atomTypes[i]); */
    
/*     for (j = 0; j < cryInput->numAtoms; j++) { */
/*       r = Vector3D_distance(cryInput->atomLocations[i], */
/* 			    cryInput->atomLocations[j]); */
/*       real qr = r * q;///(40.0 * M_PI); */
      
/*       f1 = 0.0; */
/*       f2 = 0.0; */
/*       startIndex1 = NUM_TERMS * cryInput->atomTypes[i]; */
/*       startIndex2 = NUM_TERMS * cryInput->atomTypes[j]; */
/*       for (k = 0; k < NUM_TERMS; k++) { */
/* 	f1 += Aterm[startIndex1+k] * exp(-Bterm[startIndex1+k]* sinThetaOverLambda*sinThetaOverLambda); */
/* 	f2 += Aterm[startIndex2+k] * exp(-Bterm[startIndex2+k]* sinThetaOverLambda*sinThetaOverLambda); */
/*       } */
/* #ifdef MADMAX */
/*       if ((cryInput->atomTypes[i] == 33) || (cryInput->atomTypes[i] == 25)) { */
/* 	printf("atom %d is selenium or iron!\n", j); */
/* 	f1 = f1 + fprime + I * fdoubleprime; */
/*       } else { */
/* 	f1prime = 0.; */
/* 	f1doubleprime = 0.; */
/*       } */
      
/*       if ((cryInput->atomTypes[j] == 33) || (cryInput->atomTypes[j] == 25)) { */
/* 	printf("atom %d is selenium or iron!\n", j); */
/* 	f2 +=fprime + I * fdoubleprime; */
/* 	/\* 		  real incidentAtEdge = 7110.0;  // Fe edge *\/ */
/* 	/\* 		  real incidentOffEdge = incidentAtEdge - 50.0; // Fe off edge, -50 *\/ */
/* 	/\* 		  getAnomalousParameters(26, incidentEnergy, &f2prime, &f2doubleprime); *\/ */
/*       } else { */
/* 	f2prime = 0.; */
/* 	f2doubleprime = 0.; */
/*       } */
      
/*       /\* 		if (0) { *\/ */
/*       /\* /\\* 		  printf("setting f1, f2 and prime and double primes to bogus things for testing!\n"); *\\/ *\/ */
/*       /\* 		  if (i==0) { *\/ */
/*       /\* 			 f1 = 0.2;  f1prime = 1; f1doubleprime = 1.5 - .4 * q;//f1prime = 0.4;  f1doubleprime = 0.6; *\/ */
/*       /\* 		  } else { *\/ */
/*       /\* 			 f1 = 1.0;  f1prime = 2; f1doubleprime = 2;//f2prime = 2.0;  f2doubleprime = 3.0; *\/ */
/*       /\* 		  } *\/ */
/*       /\* 		  if (j==0) { *\/ */
/*       /\* 			 f2 = 0.2;  f2prime = 1; f2doubleprime = 1.5 - .4 * q;//f1prime = 0.4;  f1doubleprime = 0.6; *\/ */
/*       /\* 		  } else { *\/ */
/*       /\* 			 f2 = 1.0;  f2prime = 2; f2doubleprime = 2;//f2prime = 2.0;  f2doubleprime = 3.0; *\/ */
/*       /\* 		  } *\/ */
      
/*       /\* 		  f1 = f1 + f1prime + I * f1doubleprime; *\/ */
/*       /\* 		  f2 = f2 + f2prime + I * f2doubleprime; *\/ */
/*       /\* 		} *\/ */
      
      
/*       f2 = conj(f2); */
/* #endif		 */
/*       if (i == j) { */
/* 	cIqDebyeOnly += f1*f2; */
/*       } else { */
/* 	cIqDebyeOnly += f1 * f2 * sin(qr) / qr; */
/*       } */
      
/*       real cubeRootVol1 = cryInput->atomGaussianRadii[i];  // note set to atomGaussianRadii */
/*       real cubeRootVol2 = cryInput->atomGaussianRadii[j]; */
/*       real fracA = 2.0 * M_PI * 2.0 * M_PI; */
/*       real FMSContribution1 = -proteinDensity * cubeRootVol1 * cubeRootVol1 * cubeRootVol1 */
/* 	* exp(- M_PI * cubeRootVol1 * cubeRootVol1 *  s * s / fracA); */
/*       real FMSContribution2 = -proteinDensity * cubeRootVol2 * cubeRootVol2 * cubeRootVol2  //Gaussian exclFMS */
/* 	* exp(- M_PI * cubeRootVol2 * cubeRootVol2 *  s * s / fracA); */
      
/*       if (i == j) { */
/* 	cIqGaussianFMS += (FMSContribution1) * (FMSContribution2); */
/* 	cIqDebyePlusGaussian += (f1+FMSContribution1)*(f2+FMSContribution2); */
/*       } else { */
/* 	cIqGaussianFMS += (FMSContribution1) * (FMSContribution2) * sin(qr) / qr; */
/* 	cIqDebyePlusGaussian += (f1+FMSContribution1)*(f2+FMSContribution2) * sin(qr) / qr; */
/*       } */
      
/*       real r1 = cryInput->atomHardSphereRadii[i]; // cubeRootVol1/pow(4.0*M_PI/3.0,1./3.); // note set to atomHardSphereRadii */
/*       real r2 = cryInput->atomHardSphereRadii[j]; // cubeRootVol2/pow(4.0*M_PI/3.0,1./3.); */
/*       FMSContribution1 = -proteinDensity * 4*M_PI * (sin(r1*s)-r1*s*cos(r1*s))/pow(s,3.0); // Hard sphere FMS */
/*       FMSContribution2 = -proteinDensity * 4*M_PI * (sin(r2*s)-r2*s*cos(r2*s))/pow(s,3.0); // Hard sphere */
/*       if (i == j) { */
/* 	cIqHardSphereFMS += (FMSContribution1) * (FMSContribution2); */
/* 	cIqDebyePlusHS += (f1+FMSContribution1)*(f2+FMSContribution2); */
/*       } else { */
/* 	cIqHardSphereFMS += (FMSContribution1) * (FMSContribution2) * sin(qr) / qr; */
/* 	cIqDebyePlusHS += (f1+FMSContribution1)*(f2+FMSContribution2) * sin(qr) / qr; */
/*       } */
/*     } */
/*   } */
  
/*   *IqDebyeOnly = creal(cIqDebyeOnly); */
/*   *IqDebyePlusHS = creal(cIqDebyePlusHS); */
/*   *IqDebyePlusGaussian = creal(cIqDebyePlusGaussian); */
/*   *IqHardSphereFMS = creal(cIqHardSphereFMS); */
/*   if (abs(cimag(cIqDebyePlusGaussian)) > 1e-10) { */
/*     printf("why is imaginary component not zero!?! %lf\n", cimag(cIqDebyePlusGaussian)); */
/*     exit(-1); */
/*   } */

/* } */


/* void GetDebyeOnly(PBEproblem problem, CRY_input cryInput, */
/* 		  real proteinDensity, unsigned int numpoints, */
/* 		  real *qList, real *IqDebyeOnly, real *IqHardSphereFMS, */
/* 		  real *IqGaussianFMS, real *IqDebyePlusHS, real *IqDebyePlusGaussian) { */
/*   unsigned int i, j; */
/*   for (i = 0; i < numpoints; i++) { */
/*     getDebyeAtParticularAngle(problem, cryInput, proteinDensity, qList[i], &(IqDebyeOnly[i]), */
/* 			      &(IqHardSphereFMS[i]), &(IqGaussianFMS[i]), &(IqDebyePlusHS[i]), &(IqDebyePlusGaussian[i])); */
/*   } */
/* } */

void evalAtomicScattering(ScatterOptions options, PBEproblem problem, ScatterParameters parameters, Vector3D kOrigVector, real proteinDensity, complexreal *atomic, complexreal *excl)
{
  real sToSinThetaOverLambda = 1/(4.0 * M_PI);
  Vector3D kVector = Vector3D_allocate();
  Vector3D_copy(kVector, kOrigVector);
  real s = Vector3D_length(kVector);
  Vector3D_scale(kVector, sToSinThetaOverLambda);
  complexreal atomicContribution = 0.0;
  complexreal curAtomicContribution = 0.0;
  complexreal FMSContribution = 0.0;
  Vector3D currentPoint;
  real *Aterm;
  real *Bterm;
  unsigned int i, j, startIndex;
  real sinThetaOverLambda = Vector3D_length(kVector);
  if (sinThetaOverLambda < 2.0) {
	 Aterm = parameters->lowA;
	 Bterm = parameters->lowB;
  } else if (sinThetaOverLambda < 4.0) {
	 Aterm = parameters->medA;
	 Bterm = parameters->medB;
  } else if (sinThetaOverLambda < 6.0) {
	 Aterm = parameters->highA;
	 Bterm = parameters->highB;
  } else {
	 printf("evalAtomicScattering: magnitude of k-vector is an illegal %f.\n", (float)sinThetaOverLambda);
	 exit(-1);
  }

  for (i = 0; i < parameters->numAtoms; i++) {
	 curAtomicContribution = 0.0;
	 currentPoint = parameters->atomLocations[i];
	 startIndex = NUM_TERMS * parameters->atomTypes[i];
	 for (j = 0; j < NUM_TERMS; j++) {
		curAtomicContribution += Aterm[startIndex+j] * exp(-Bterm[startIndex+j] * sinThetaOverLambda * sinThetaOverLambda);
	 }


/* #ifdef MADMAX	  */
/* 	 if ((cryInput->atomTypes[i] == 25) || (cryInput->atomTypes[i] == 33)) { */
/* 		curAtomicContribution += fprime + I * fdoubleprime; */
/* 	 } */
/* #endif */

	 real cubeRootVol = parameters->atomGaussianRadii[i];
	 real r = cubeRootVol/pow(4.0*M_PI/3.0,1./3.);
	 real fracA = 2.0 * M_PI * 2.0 * M_PI;
	 real scaledR = options->Rm_scale * parameters->aveAtomicRadius;

/* 	 // first FMSContribution calculation (below) is for a Gaussian excl vol function */
	 real fourThirdsPiToTwoThirds = pow(4.0 * M_PI/3.0, 2./3.);
	 real groupAdjustment = ((scaledR * scaledR * scaledR) / (parameters->aveAtomicRadius * parameters->aveAtomicRadius * parameters->aveAtomicRadius))
		     * exp( - fourThirdsPiToTwoThirds * (scaledR * scaledR - parameters->aveAtomicRadius  * parameters->aveAtomicRadius) * M_PI * s * s /fracA );
	 FMSContribution -= proteinDensity * cubeRootVol * cubeRootVol * cubeRootVol
		* exp(- M_PI * cubeRootVol * cubeRootVol *  s * s / fracA)
		* cexp(-I * Vector3D_dot(kOrigVector, currentPoint)) * groupAdjustment;

/* 	 // the below FMSContribution calculation is for a hard sphere */
/* /\* 	 FMSContribution += proteinDensity * 4 * M_PI * ((sin(r*s)-r*s*cos(r*s))/pow(s,3.0)) *\/ */
/* /\* 		* cexp(-I * Vector3D_dot(kOrigVector, currentPoint)); *\/ */
	 
	 /* if (useBFactors) { */
	 /* 	//		printf("Using B factors! factor is %f\n",(float) (-1.0 * cryInput->atomTemperatureFactors[i] * s * s / (16.0 * M_PI * M_PI))); */
	 /* 	curAtomicContribution *= exp(-1.0 * parameters->atomTemperatureFactors[i] * s * s / (16 * M_PI * M_PI)); */
	 /* } else { */
	 /* 	//		printf("NOT using B factors!\n"); */
	 /* } */
	 
 	 atomicContribution += cexp(-I* Vector3D_dot(kOrigVector, currentPoint)) * curAtomicContribution ;
  }

  Vector3D_scale(kVector, 1./ sToSinThetaOverLambda);
  Vector3D_free(kVector);
  *atomic = atomicContribution;
  *excl = FMSContribution;
}

/* void handleOverlappingHardSpheres(PBEproblem problem, CRY_input cryInput, unsigned int i, unsigned int j) */
/* { */
/*   if (! (i <= j) ) {  // MUST satisfy this rule in order for construct */
/* 	 assert(i <= j); */
/*   } */

/*   unsigned int p,q; */
  
  
// /*   for (p = 0; p < cubesFromSph_i->numCubes; p++) { */
// /* 	 // skip if already overlapped! */
// /* 	 if (cubesFromSph_i->isOverlapped[p]) { */
// /* 		continue; */
// /* 	 } */

//	 // skip if sphCube[p] doesn't overlap sphere j
// /* 	 if (Vector3D_distance(cubesFromSph_i->cubeCenters[p], cryInput->atomLocations[j]) */
// /* 		  > sqrt3over2 * cubesFromSph_i->cubeSize + cryInput->atomHardSphereRadii[j]) { */
// /* 		continue; */
// /* 	 } */
// 	 
// 	 // sphCube[p] _does_ overlap sphere j;  find the appropriate sphcube in sph j
// /* 	 for (q = 0; q < cubesFromSph_j->numCubes; q++) { */
// 		
// /* 	 } */
// /*   } */
/* } */

#include "config.h"
#include "stdio.h"
#include "stdlib.h"
#include "softwaxs.h"

int main(int argc, char* argv[]) {  
  setlinebuf(stdout);

  ScatterOptions options = ScatterOptions_allocate();
  ScatterOptions_parseArgs(options, argc, argv);
  PBEproblem problem = PBEproblem_allocate(options->PDB, options->SRF, options->resolution);
  initializeSurfOperatorTreesWithScatterData(problem, options->excludedVolumeDensity, options->hydrationShellDensity);

  ScatterParameters parameters = ScatterParameters_allocate(options, problem);
  ScatterIntensities intensities = ScatterIntensities_allocate(options, problem);

  real totalScatteringVolume = scatterEvalVolume(problem->pbesurfaceoperator->children[0]->tree->root);
  printf("scattering volume of first dielectric is %lf\n",totalScatteringVolume);

  calculateScatteringPattern(options, problem, parameters, intensities);

 /* GetDebyeOnly(options, problem, parameters, intensities); */
  /* // cryInput, (real)atof(argv[7]), numQpoints, sAngInv, IsDebye, IsHardSphere, IsGaussian, IsDebyePlusHS, IsDebyePlusGaussian); */
  ScatterIntensities_output(options->outputFile, intensities);

  // clean up, go home
  ScatterIntensities_free(intensities);
  ScatterParameters_free(parameters);
  ScatterOptions_free(options);
  PBEproblem_free(problem);
  return 0;
}

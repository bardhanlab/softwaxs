#include "unistd.h"
#include "getopt.h"
#include "softwaxs.h"

ScatterOptions ScatterOptions_allocate() {
  ScatterOptions options = NULL;
  options = (ScatterOptions)(calloc(1, sizeof(_ScatterOptions)));
  ScatterOptions_setDefaults(options);
  return options;
}

void ScatterOptions_free(ScatterOptions options) {
  free(options->PDB);
  free(options->SRF);
  free(options->SIZ);
  free(options->CRY);
  free(options->anomalousFile);
  free(options->parameterDirectory);
  free(options->outputFile);
  free(options->paramFile);
  free(options);
}

void ScatterOptions_setDefaults (ScatterOptions options) {
  options->PDB = NULL;
  options->SRF = NULL;
  options->outputFile = NULL;
  options->SIZ = NULL;
  options->CRY = NULL;
  options->parameterDirectory = NULL;
  options->anomalousFile = NULL;
  options->paramFile = NULL;
  
  options->resolution = 6;
  options->useBfactors = 0;
  options->Rm_scale = 1.0;
  options->numSphQuad = 900;

  options->minQ = 0.005;
  options->maxQ = 1.995;
  options->numQpoints = 200;

  options->excludedVolumeDensity = 0.334;
  options->hydrationShellDensity = 0.03;

  options->useMADMAX = 0;
}

void ScatterOptions_loadConfigFile(ScatterOptions options, char *configFile) {
  char line[128];
  char key[128];
  char val[128];
  
  FILE *infile = fopen(configFile,"r");

  if (infile == NULL) {
    printf("Error in ScatterOptions_loadConfigFile: Could not open config file %s.\n", configFile);
    exit(-1);
  }

  while(!feof(infile)) {
    fgets(line, 128, infile);
    sscanf(line, "%s %s",key,val);
    if (! strcmp("lowA",key)) {
      options->lowAfile = strdup(val);
    }
    if (! strcmp("medA",key)) {
      options->medAfile = strdup(val);
    }
    if (! strcmp("highA",key)) {
      options->highAfile = strdup(val);
    }
    if (! strcmp("lowB",key)) {
      options->lowBfile = strdup(val);
    }
    if (! strcmp("medB",key)) {
      options->medBfile = strdup(val);
    }
    if (! strcmp("highB",key)) {
      options->highBfile = strdup(val);
    }
  }
	
  fclose(infile);
}

void ScatterOptions_parseArgs(ScatterOptions options, int argc, char** argv) {

  optind = 1;
    
  while (1) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
      {"help",    no_argument,           0,  0 },
      {"config",  required_argument,     0,  0 },
      {"prmdir",  required_argument,     0,  0 },
      {"param",   required_argument,     0,  0 },
      {"siz",     required_argument,     0,  0 },
      {"pdb",     required_argument,     0,  0 },
      {"srf",     required_argument,     0,  0 },
      {"cry",     required_argument,     0,  0 },
      {"output",  required_argument,     0,  0 },
      {"res",     required_argument,     0,  0 },
      {"madmax",  no_argument,           0,  0 },
      {"energy",  required_argument,     0,  0 },
      {"anomalous", required_argument,   0,  0 },
      {"protdens",required_argument,     0,  0 },
      {"hydrdens",required_argument,     0,  0 },
      {"Rmscale", required_argument,     0,  0 },
      {"qmin",    required_argument,     0,  0 },
      {"qmax",    required_argument,     0,  0 },
      {"numqpts", required_argument,     0,  0 },
      {"useBfactors", no_argument,       0,  0 },
      {"numsphquad",  required_argument, 0,  0 },
      {0,         0,                     0,  0 }
    };

    char c = getopt_long(argc, argv, "", long_options, &option_index);
    if (c == -1)
      break;

    if (! strcmp("help", long_options[option_index].name)) {
      printf("Usage: %s [--help] [--config softwaxs.cfg] --param waxs.prm --siz radii.siz --pdb protein.pdb --cry protein.cry\n", argv[0]);
      printf("\t--srf protein.srf --output output.txt [--res 6] [--madmax] [--energy photonEnergy] [--anomalous scatter.dat]\n");
      printf("\t[--protdens 0.334] [--hydrdens 0.03] [--Rmscale 1.0] [--qmin 0.01] [--qmax 2.00] [--numqpts 200]\n");
      printf("\t[--useBfactors] [--numsphquad 900]\n");
      exit(0);
    }

    if (! strcmp("config", long_options[option_index].name)) {
      char *configFile = NULL;
      configFile = strdup(optarg);
      ScatterOptions_loadConfigFile(options, configFile);
      free(configFile);
    }

    if (! strcmp("prmdir", long_options[option_index].name)) { options->parameterDirectory = strdup(optarg); }

    if (! strcmp("param", long_options[option_index].name)) { options->paramFile = strdup(optarg); }
    if (! strcmp("siz", long_options[option_index].name)) { options->SIZ = strdup(optarg); }
    if (! strcmp("pdb", long_options[option_index].name)) { options->PDB = strdup(optarg); }
    if (! strcmp("srf", long_options[option_index].name)) { options->SRF = strdup(optarg); }
    if (! strcmp("cry", long_options[option_index].name)) { options->CRY = strdup(optarg); }
    if (! strcmp("output", long_options[option_index].name)) { options->outputFile = strdup(optarg); }
    if (! strcmp("res", long_options[option_index].name)) { options->resolution = atoi(optarg); }
    if (! strcmp("madmax", long_options[option_index].name)) { options->useMADMAX = 1; }
    if (! strcmp("energy", long_options[option_index].name)) { options->energy = (real)atof(optarg); }
    if (! strcmp("anomalous", long_options[option_index].name)) { options->anomalousFile = strdup(optarg); }
    if (! strcmp("protdens", long_options[option_index].name)) { options->excludedVolumeDensity = (real)atof(optarg);}
    if (! strcmp("hydrdens", long_options[option_index].name)) { options->hydrationShellDensity = (real)atof(optarg);}
    if (! strcmp("Rmscale", long_options[option_index].name)) { options->Rm_scale = (real)atof(optarg);}
    if (! strcmp("qmin", long_options[option_index].name)) { options->minQ = (real)atof(optarg);}
    if (! strcmp("qmax", long_options[option_index].name)) { options->maxQ = (real)atof(optarg);}
    if (! strcmp("numqpts", long_options[option_index].name)) { options->numQpoints = (real)atof(optarg);}
    if (! strcmp("useBfactors", long_options[option_index].name)) { options->useBfactors = 1;}
    if (! strcmp("numsphquad", long_options[option_index].name)) { options->numSphQuad = atoi(optarg);}
  }

  // this should be split into another function...
  // now check that our combinations of options are legal
  if (options->paramFile == NULL) {
    printf("Warning: parameter file not specified with --param!\n");
  }
  if (options->SIZ == NULL) {
    printf("Error: Radii file not specified with --siz!\n");
    exit(-1);
  }
  if (options->PDB == NULL) {
    printf("Error: PDB file not specified with --pdb!\n");
    exit(-1);
  }
  if (options->outputFile == NULL) {
    printf("Error: output file not specified with --output!\n");
    exit(-1);
  }
  if (options->SRF == NULL) {
    printf("Error: Surface file not specified with --srf!\n");
    exit(-1);
  }
  if (options->CRY == NULL) {
    printf("Error: CRY file not specified with --cry!\n");
    exit(-1);
  }
  if ((options->resolution < MIN_RESOLUTION) || (options->resolution > MAX_RESOLUTION)) {
    printf("Error: Resolution %d is not in legal range %d - %d!\n", options->resolution, MIN_RESOLUTION, MAX_RESOLUTION);
    exit(-1);
  }
  if (options->useMADMAX) {
    printf("Error: For right now, anomalous scattering calculations are disabled!\n");
    exit(-1);
  }
  if (options->useMADMAX && (options->anomalousFile == NULL)) {
    printf("Error: Anomalous scattering calculations require specification of the anomalous scatterer data with --anomalous!\n");
    exit(-1);
  }
  if (options->useMADMAX && (options->anomalousFile != NULL)) {
    printf("Error: I need to check that the photon energy for anomalous scattering is legal!  This is not implemented yet.\n");
    exit(-1);
  }
  if (options->excludedVolumeDensity < MIN_DENSITY || options->excludedVolumeDensity > MAX_DENSITY) {
    printf("Error: Excluded volume density %lf is not in legal range (%lf, %lf).\n", options->excludedVolumeDensity, MIN_DENSITY, MAX_DENSITY);
    exit(-1);
  }
  if (options->hydrationShellDensity < MIN_DENSITY || options->hydrationShellDensity > MAX_DENSITY) {
    printf("Error: Hydration shell density %lf is not in legal range (%lf, %lf).\n", options->hydrationShellDensity, MIN_DENSITY, MAX_DENSITY);
    exit(-1);
  }
  if (options->minQ < MIN_Q || options->minQ > MAX_Q) {
    printf("Error: minimum q %lf is not in legal range (%lf, %lf).\n", options->minQ, MIN_Q, MAX_Q);
    exit(-1);
  }
  if (options->maxQ < options->minQ) {
    printf("Error: maximum q %lf is less than minimum q %f.\n", options->maxQ, options->minQ);
    exit(-1);
  }
  if (options->maxQ > MAX_Q) {
    printf("Error: maximum q %lf is not legal range (%lf, %lf).\n",options->maxQ, MIN_Q, MAX_Q);
    exit(-1);
  }
  if (options->numQpoints < MIN_NUM_Q || options->numQpoints > MAX_NUM_Q) {
    printf("Error: number of q points %d is not in legal range (%d, %d).\n", options->numQpoints, MIN_NUM_Q, MAX_NUM_Q);
    exit(-1);
  }
  if (options->useBfactors) {
    printf("Error: use of B factors is not yet enabled!\n");
    exit(-1);
  }
  if (options->numSphQuad < MIN_NUM_SPH_QUAD || options->numSphQuad > MAX_NUM_SPH_QUAD) {
    printf("Error: number of spherical quadrature points %d is not in legal range (%d, %d).\n", options->numSphQuad, MIN_NUM_SPH_QUAD, MAX_NUM_SPH_QUAD);
    exit(-1);
  }
}
  /* char paramFile[100]; */
  /* char *parameterDirectory = getenv("MM_PARAMETER_DIR"); */
  /* if (parameterDirectory == NULL) { */
  /* 	 printf("Environment variable \"MM_PARAMETER_DIR\" is not set!\nAssuming parameter files have full path information...\n"); */
  /* 	 sprintf(paramFile, "%s", argv[1]); */
  /* } else { */
  /* 	 sprintf(paramFile, "%s/%s", parameterDirectory, argv[1]); */
  /* } */
  /* sprintf(paramFile, "%s", argv[1]); */

  /* if (parameterDirectory == NULL) { */
  /* 	 sprintf(sizFile, "%s", argv[2]); */
  /* } else { */
  /* 	 sprintf(sizFile, "%s/%s", parameterDirectory, argv[2]); */
  /* } */

  /* return options; */

  /*   } */


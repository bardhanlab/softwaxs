#include "FFTSVD.h"
 
/* Constructors and Destructors */

Panel Panel_allocate() {
   return (Panel)calloc(1, sizeof(_Panel));
}

void Panel_free(Panel panel) {
   free(panel);
}

/* Operators */
void Panel_FlatPanel(Panel panel, FlatPanel flatpanel) {
   panel->centroid = flatpanel->centroid;
   panel->normal = flatpanel->panelaxis[2];

   panel->numdirectquadpoints = flatpanel->numdirectquadpoints;
   panel->directquadpoints = flatpanel->directquadpoints;
   panel->directquadnormals = flatpanel->directquadnormals;
   panel->directquadweights = flatpanel->directquadweights;

   Vector3D point = Vector3D_allocate();
   point->x = 1000000.0;
   //panel->area = Integration(point, panel, CONSTANT_KERNEL, NULL, SINGLE_LAYER_INT);
   panel->area = flatpanel->area;
   panel->type = FLAT_PANEL;
   panel->realpanel = flatpanel;

   Vector3D_free(point);
}

void Panel_Vector3D(Panel panel, Vector3D v3d) {
   panel->centroid = v3d;
   panel->normal = NULL;
   panel->type = POINT_PANEL;
   panel->realpanel = v3d;
   panel->area = 0.0;
}

real Panel_memory(Panel panel, BEMLayerType layertype) {
   switch (panel->type) {
   case FLAT_PANEL: return FlatPanel_memory(4, layertype);
   default: return 0;
   }
}

real Panel_quadrature(Vector3D point, Panel panel, BEMKernelType kerneltype, void* parameters, BEMLayerType layertype) {
   unsigned int i;
   real integral = 0.0;

   if (layertype == SINGLE_LAYER_INT) {
      for (i = 0; i < panel->numdirectquadpoints; i++)
	integral += 0.0;
   }
   else if (layertype == DOUBLE_LAYER_INT) {
      for (i = 0; i < panel->numdirectquadpoints; i++)
	integral -= 0.0;
   }

   return integral;
}

#include "softwaxs.h"

ScatterIntensities ScatterIntensities_allocate(ScatterOptions options, PBEproblem problem) {
  ScatterIntensities intensities = NULL;
  intensities = (ScatterIntensities)(calloc(1, sizeof(_ScatterIntensities)));
  intensities->minQ          = options->minQ;
  intensities->maxQ          = options->maxQ;
  intensities->numQpoints    = options->numQpoints;
  intensities->sAngInv       = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsAngInv      = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsFMSAngInv   = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->DebyeAngInv   = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->ExclAngInv    = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->BorderAngInv  = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->FMSExcl       = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->testIsAngInv  = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsDebye       = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsDebyePlusHS = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsHardSphere  = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsGaussian    = (real *)calloc(intensities->numQpoints, sizeof(real));
  intensities->IsDebyePlusGaussian = (real *)calloc(intensities->numQpoints, sizeof(real));
  unsigned int i;
  for (i = 0; i < intensities->numQpoints; i++) {
    intensities->sAngInv[i] = intensities->minQ+((intensities->maxQ-intensities->minQ)/(intensities->numQpoints-1))*(real)i;
    intensities->IsAngInv[i] = 0.0;
    intensities->DebyeAngInv[i] = 0.0;
    intensities->ExclAngInv[i] = 0.0;
    intensities->BorderAngInv[i] = 0.0;
  }
  return intensities;
}

void ScatterIntensities_free(ScatterIntensities intensities) {
  free(intensities->IsHardSphere);
  free(intensities->IsGaussian);
  free(intensities->IsDebye);
  free(intensities->IsDebyePlusHS);
  free(intensities->IsDebyePlusGaussian);
  free(intensities->sAngInv);
  free(intensities->IsAngInv);
  free(intensities->IsFMSAngInv);
  free(intensities->FMSExcl);
  free(intensities->ExclAngInv);
  free(intensities->BorderAngInv);
  free(intensities->DebyeAngInv);
  free(intensities);
}

void ScatterIntensities_output(char *filename, ScatterIntensities intensities) {
  FILE *outfile = fopen(filename, "w");
  if (outfile == NULL) {
    printf("ScatterIntensities_output: Could not open output file %s.  dying...\n",filename);
    exit(-1);
  }
  unsigned int i;
  for (i = 0; i < intensities->numQpoints; i++) {
    fprintf(outfile, "%f    %f    %f    %f    %f    %f    %f    %f    %f    %f    %f    %f\n", (float)intensities->sAngInv[i],
	    (float)intensities->IsAngInv[i], (float)intensities->DebyeAngInv[i],
	    (float)intensities->ExclAngInv[i], (float)intensities->BorderAngInv[i],
	    (float)intensities->IsFMSAngInv[i], (float)intensities->FMSExcl[i],
	    (float)intensities->IsDebye[i], (float)intensities->IsHardSphere[i], (float)intensities->IsGaussian[i],
	    (float)intensities->IsDebyePlusHS[i], (float)intensities->IsDebyePlusGaussian[i]);
  }
  fclose(outfile);
}

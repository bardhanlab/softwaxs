#ifndef __PBEPROBLEM_H__
#define __PBEPROBLEM_H__

#include "FFTSVDpbeAPI.h"

typedef struct _PBEproblem {
   unsigned int numsalts;
   Panel** saltpanels;
   unsigned int* numsaltpanels;

   unsigned int numdielectrics;
   Panel** dielectricpanels;
   unsigned int* numdielectricpanels;
   unsigned int* dielectricparent; 

   unsigned int numdielectriccavities;
   Panel** dielectriccavitypanels;
   unsigned int* numdielectriccavitypanels;
   unsigned int* dielectriccavityparent;

   unsigned int numsaltcavities;
   Panel** saltcavitypanels;
   unsigned int* numsaltcavitypanels;
   unsigned int* saltcavityparent;

   unsigned int numtotalpanels;
   unsigned int numtotalsurfacevariables; //  == 2 * numtotalpanels in green's thm,
   // but only 1 * numtotalpanels in qualocation
   
   /* Surface Operator Related */
   SurfaceOperator pbesurfaceoperator;  /* The big momma */

   PDBentry* pdbentries;
   unsigned int numpdbentries;

   unsigned int numvariablecharges;
   unsigned int *variablechargeindextoglobalindex;
   unsigned int numfixedligandcharges;
   unsigned int *fixedligandchargeindextoglobalindex;
   unsigned int numfixedreceptorcharges;
   unsigned int *fixedreceptorchargeindextoglobalindex;

   Vector globalCharges;
   Vector RHS;
   Vector Sol;
   Vector globalPhiReact;
   Vector variablePhiReact;

   char *directory;  /* the bound and unbound PDB files are in
                        separate directories, usually */

} _PBEproblem;

typedef struct _PBEproblem* PBEproblem;

PBEproblem PBEproblem_allocate(char *PDBfilename, char *SRFfilename, unsigned int resolution);
void PBEproblem_free(PBEproblem problem);
void PBEproblem_allocateButDontCompress(PBEproblem problem, char *PDBfilename, char *SRFfilename);
void PBEproblem_initialize(PBEproblem problem, unsigned int resolution);
PBEproblem PBEproblem_loadOnlyPDB(char *PDBfilename);

#endif

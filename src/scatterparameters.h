#ifndef __SCATTERPARAMETERS_H__
#define __SCATTERPARAMETERS_H__

typedef struct _ScatterParameters {
  real aveAtomicRadius;  // not sure this belongs here...

  // basic spherical quadrature--this used to be in sphQuad
  unsigned int numSphQuadPoints;
  Vector3D*    sphQuadPoints;
  real*        sphQuadWeights;

  // molecular information--this used to be in CRYinput
  unsigned int  numAtoms;
  unsigned int *atomTypes;
  real         *atomMass;
  real         *atomVolume;
  Vector3D     *atomLocations;
  real         *atomTemperatureFactors;
  real         *atomHardSphereRadii;
  real         *atomGaussianRadii;
  
  // atomic form factors
  real         *lowA, *lowB;
  real         *medA, *medB;
  real         *highA, *highB;

  // MAD MAX
  unsigned int useMADMAX;
  real incidentEnergy;
  real fprime, fdoubleprime;
  unsigned int *numEnergies;
  real **energies;
  real **anomalousFprime;
  real **anomalousFdoubleprime;

} _ScatterParameters;

typedef _ScatterParameters* ScatterParameters;
ScatterParameters ScatterParameters_allocate(ScatterOptions options, PBEproblem problem);
void ScatterParameters_free(ScatterParameters parameters);

void ScatterParameters_CRYload(ScatterParameters parameters, PBEproblem problem, char *filename);
void ScatterParameters_freeCRY(ScatterParameters parameters);

void ScatterParameters_readCoeffs(ScatterParameters parameters, ScatterOptions options);
void ScatterParameters_freeCoeffs(ScatterParameters parameters);

void ScatterParameters_initSphQuad(ScatterParameters parameters, ScatterOptions options, unsigned int d);
void ScatterParameters_freeSphQuad(ScatterParameters parameters);
void ScatterParameters_quadCartToSph2(real* rho, real* theta, real* phi, Vector3D pnt);
void ScatterParameters_quadSphToCart2(Vector3D pnt, real rho, real theta, real phi);

void ScatterParameters_anomalousInit(ScatterParameters parameters);
void ScatterParameters_anomalousLoad(ScatterParameters parameters, unsigned int atomicNumber, char *filename);
void ScatterParameters_anomalousGetParameters(ScatterParameters parameters, unsigned int atomicNumber, real energy, real *fprime, real *fdoubleprime);
void ScatterParameters_freeAnomalous(ScatterParameters parameters);


#endif

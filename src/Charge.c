#include "FFTSVD.h"

/* Constructors and Destructors */

Charge Charge_allocate() {
   return (Charge)calloc(1, sizeof(_Charge));
}

void Charge_free(Charge charge) {
   unsigned int i;
   
   for (i = 0; i < charge->numcharges; i++)
      Vector3D_free(charge->points[i]);
   
   free(charge->points);
   Vector_free(charge->charges);

   free(charge);
}

/* Operations */

void Charge_read(Charge charge, FILE* file) {
   char line[128];
   unsigned int i;
        
   charge->numcharges = 0;
    
   while (fgets(line, 128, file))
      charge->numcharges++;
       
   charge->points = (Vector3D*)calloc(charge->numcharges, sizeof(Vector3D));
   charge->charges = Vector_allocate(charge->numcharges);
    
   rewind(file);
    
   for (i = 0; i < charge->numcharges; i++) {
      charge->points[i] = Vector3D_allocate();

      fgets(line, 128, file);
      sscanf(line, "%lf %lf %lf %lf", &charge->points[i]->x, &charge->points[i]->y, &charge->points[i]->z, &charge->charges[i]);
   }
}


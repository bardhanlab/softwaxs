#include "FFTSVD.h"

real Integration(Vector3D point, Panel panel, BEMKernelType kerneltype, void* parameters, BEMLayerType layertype) {
   real slp = 0.0, dlp = 0.0;

   switch (panel->type) {
   case FLAT_PANEL: {
     switch (kerneltype) {
     case CONSTANT_KERNEL: slp = panel->area; break;
     case POISSON_KERNEL: FlatIntegration_oneoverr(point, (FlatPanel)(panel->realpanel), parameters, &slp, &dlp); break;
     }
     break;
   }
   }
   
   switch (layertype) {
   case SINGLE_LAYER_INT: return slp;
   case DOUBLE_LAYER_INT: return dlp;
   default: return 0.0;
   }
}

/* Typedef */

typedef struct _Tree {
   Panel* panels;  /* Panels stored in this tree */
   unsigned int numpanels;  /* Size of panels */
   Vector3D* points;  /* Points stored in this tree */
   Vector3D* normals;  /* Normals stored in this tree -- added 9/21/08 by JPB for discret. paper*/
   unsigned int numpoints;  /* Size of points, also size of normals */
   unsigned int maxpanelsperfinestcube;  /* How many panels can be in a cube on the finest level */
   unsigned int partitioningdepth;  /* How many levels of cubes */
   BEMKernelType kerneltype;  /* The kernel */
   void* parameters;  /* Parameters passed to Green's function and integrator */
   unsigned int gridpoints;  /* gridpoints^3 is the minimum FFT grid size */
   unsigned int* gridpointsperlevel;  /* number of gridpoints actually used per level >= gridpoints */

   real epsilon;  /* SVD precision, relative tolerance */
   BEMLayerType layertype;  /* Single or double layer is needed */
   Cube root;  /* Root node of the cube hierarchy */
   real minimumcubesize;  /* Smallest allowable dimension for a cube */
} _Tree;

typedef _Tree* Tree;

/* Constructors and Destructors */

Tree Tree_allocate(unsigned int resolution, Panel* panels, unsigned int numpanels, Vector3D* points, unsigned int numpoints, unsigned int maxpanelsperfinestcube, BEMKernelType kerneltype, void* parameters, unsigned int gridpoints, real epsilon, BEMLayerType layertype, real minimumcubesize);
void Tree_free(Tree tree);

/* Operations */

void Tree_lists(Tree tree);
void Tree_memory(Tree tree);

#include "scattercube.h"
#include "sphQuad.h"

real excludedVolumeDensity;
real hydrationLayerDensity;


// based on implementation in
// http://www.softsurfer.com/Archive/algorithm_0105/algorithm_0105.htm
unsigned int rayIntersectsTriangle(Vector3D dir, Vector3D pointStart, Panel panel, Vector3D testIntersection) {
  Vector3D u = Vector3D_allocate();
  Vector3D v = Vector3D_allocate();
  Vector3D w0 = Vector3D_allocate();
  Vector3D w =  Vector3D_allocate();
  FlatPanel fp = (FlatPanel)panel->realpanel;
  real r, a, b;
  Vector3D_sub(u, fp->vertex[1], fp->vertex[0]);
  Vector3D_sub(v, fp->vertex[2], fp->vertex[0]);
  Vector3D_sub(w0, pointStart, fp->vertex[0]);
  a = -Vector3D_dot(panel->normal, w0);
  b = Vector3D_dot(panel->normal, dir);
  r = a / b;
  
  if (r < 0.0) {
    Vector3D_free(u);
    Vector3D_free(v);
    Vector3D_free(w0);
    Vector3D_free(w);
    return 0;
  }
  Vector3D_addscaled(testIntersection, pointStart, r, dir);
  
  real uu, uv, vv, wu, wv, D;
  uu = Vector3D_dot(u,u);
  uv = Vector3D_dot(u,v);
  vv = Vector3D_dot(v,v);
  Vector3D_sub(w, testIntersection, fp->vertex[0]);
  wu = Vector3D_dot(w,u);
  wv = Vector3D_dot(w,v);
  D = uv * uv - uu * vv;
  
  real s, t;
  s = (uv * wv - vv * wu) / D;
  if ((s < 0.0) || (s > 1.0)) {
    Vector3D_free(u);
    Vector3D_free(v);
    Vector3D_free(w0);
    Vector3D_free(w);
    return 0;
  }
  t = (uv * wu - uu * wv) / D;
  if ((t < 0.0) || ((s + t) > 1.0)) {
    Vector3D_free(u);
    Vector3D_free(v);
    Vector3D_free(w0);
    Vector3D_free(w);
    return 0;
  }

  Vector3D_free(w);
  Vector3D_free(w0);
  Vector3D_free(v);
  Vector3D_free(u);
  
  return 1;
}

void printPanel(Panel pan)
{
/*   printf("Panel->normal = (%f,%f,%f)\n", pan->normal->x,pan->normal->y, pan->normal->z); */
/*   printf("Panel->centroid = (%f,%f,%f)\n", pan->centroid->x,pan->centroid->y, pan->centroid->z); */
/*   FlatPanel fp = (FlatPanel)pan->realpanel; */
/*   printf("Panel->v1 = (%f,%f,%f)\n", fp->vertex[0]->x,fp->vertex[0]->y, fp->vertex[0]->z); */
/*   printf("Panel->v2 = (%f,%f,%f)\n", fp->vertex[1]->x,fp->vertex[1]->y, fp->vertex[1]->z); */
/*   printf("Panel->v3 = (%f,%f,%f)\n", fp->vertex[2]->x,fp->vertex[2]->y, fp->vertex[2]->z); */
}

/* this function really tests ultimately whether the surface
	intersects the LEAF CUBE surrounding the point
 */ 
int pointIsInsideSurface(SurfaceOperator surfoper, Vector3D point) {
  Cube root = surfoper->tree->root;
  Cube c = root;

  // first step: see if the point lies outside the bounding cube of the surfoper
  // if it DOES, it can't possibly be inside the surface.
  if ((point->x < c->bounds[0]->x)
      || (point->x > c->bounds[1]->x)
      || (point->y < c->bounds[0]->y)
      || (point->y > c->bounds[1]->y)
      || (point->z < c->bounds[0]->z)
      || (point->z > c->bounds[1]->z)) {
    return 0;
  }
  /*   printf("pointIsInsideSurface(), we are sure that the point is inside the surface's bounding cube \n"); */
  /*   printf("point (%f,%f,%f) should be within\n     bounds = (%f,%f,%f) - (%f,%f,%f)\n", */
  /* 			point->x, point->y, point->z, */
  /* 			c->bounds[0]->x,c->bounds[0]->y,c->bounds[0]->z, */
  /* 			c->bounds[1]->x,c->bounds[1]->y,c->bounds[1]->z); */
  unsigned int ix, iy, iz, i;
  while (! c->leaf) {
    if (point->x > c->center->x) {
      ix = 1;
    } else {
      ix = 0;
    }
    if (point->y > c->center->y) {
      iy = 1;
	 } else {
      iy = 0;
    }
    if (point->z > c->center->z) {
      iz = 1;
    } else {
      iz = 0;
    }
    if (c->children[ix][iy][iz] == NULL) {
      break; // c = parent
    } else {
      c = c->children[ix][iy][iz];
    }
  }
  Cube leafCube;
  if (c->leaf) {
    leafCube = c;
    c = c->parent;
  }
  if (c == NULL) {
    printf("pointIsInsideSurface failed!\n");
    exit(-1);
  }
  // okay, now we have in c, the parent of the leaf cube that would contain the point
  if (c->numpanelindices == 0) {
    printf("pointIsInsideSurface failed!  numpanelindices ==0?!");
    exit(-1);
  }
  
  assert(point->x >= c->bounds[0]->x);
  assert(point->x <= c->bounds[1]->x);
  assert(point->y >= c->bounds[0]->y);
  assert(point->y <= c->bounds[1]->y);
  assert(point->z >= c->bounds[0]->z);
  assert(point->z <= c->bounds[1]->z);
  /*   if (doubleCheckSurface) { */
  /* 		printf("point: (%f %f %f)\n",point->x, point->y,point->z); */
  /* 		printf("bounding box: (%f %f %f) - (%f %f %f)\n",c->bounds[0]->x,c->bounds[0]->y,c->bounds[0]->z, */
  /* 				 c->bounds[1]->x,c->bounds[1]->y,c->bounds[1]->z); */
  /*   } */
  
  /*   printf("pointIsInsideSurface(): parent of leaf cube identified"); */
  // okay.  now we're DEFINITELY in a leaf cube, "c" that surrounds
  // "point" remember: a leaf cube HAS to contain at least one panel
  // (by construction) so: take the first panel and find the ray
  // between "point" and first panel's centroid.  then look at all
  // point-panel intersections along direction ray (signed direction,
  // ie from point to firstpanel's centroid).  find closest panel
  // intersection.  check the dot product of ray and closestpanel
  // normal if its positive then "point" is inside the surface;
  // otherwise it is outside.


  // this method relies on having CLOSED SURFACES!!!

  // get ray from point to first panel on panellist
  static int initialized = 0;
  static Vector3D ray, closestPoint, testIntersection;
  if (! initialized) {
	 ray = Vector3D_allocate();
	 closestPoint = Vector3D_allocate();
	 testIntersection = Vector3D_allocate();
	 initialized = 1;
  }
  
  Vector3D_sub(ray, c->tree->panels[c->panelindices[0]]->centroid, point);

/*   if (doubleCheckSurface) { */
/* 	 printf("about to test parent-of-leaf with %d total panels\n", c->numpanelindices); */
/* 	 for (i = 0; i < c->numpanelindices; i++) { */
/* 		printf("%d\n", c->panelindices[i]); */
/* 	 } */
/* 	 printf("done listing panels.\n"); */
/* 	 printf("point = %f, %f, %f\n", point->x, point->y, point->z); */
/* 	 printf("centroid = %f, %f, %f\n", c->tree->panels[c->panelindices[0]]->centroid->x, c->tree->panels[c->panelindices[0]]->centroid->y, c->tree->panels[c->panelindices[0]]->centroid->z); */
/* 	 printf("ray = %f, %f, %f\n", ray->x, ray->y, ray->z); */
/*   } */


  unsigned int isInside = 0;
  if (Vector3D_dot(ray, c->tree->panels[c->panelindices[0]]->normal) > 0.0) {
	 isInside = 1;
/* 	 if (doubleCheckSurface) { */
/* 		printf("normals are aligned by %f\n", (float)(Vector3D_dot(ray,c->tree->panels[c->panelindices[0]]->normal)/ */
/* 																	 Vector3D_length(ray))); */
/* 		if (c->numpanelindices == 1) { */
/* 		  unsigned int cx,cy,cz; */
/* 		  cx = (leafCube->indices[0] % 2 == 0)?0:1; */
/* 		  cy = (leafCube->indices[1] % 2 == 0)?0:1; */
/* 		  cz = (leafCube->indices[2] % 2 == 0)?0:1; */
/* 		  printf("trackCube set to %d!\n", trackCube); */
/* 		  trackCube = 1; */
/* 		  printf("trackCube set to %d!\n", trackCube); */
/* 		  panelIntersectsChildCube(c->tree->panels[c->panelindices[0]], leafCube->parent, cx, cy, cz); */
/* 		  printf("trackCube set to %d!\n", trackCube); */
/* 		  printf("checking second cube leaf indices = %d %d %d!\n", leafCube->indices[0],leafCube->indices[1], leafCube->indices[2]); */
/* 		  printf("min: %f %f %f\n", leafCube->bounds[0]->x,leafCube->bounds[0]->y, leafCube->bounds[0]->z); */
/* 		  printf("max: %f %f %f\n", leafCube->bounds[1]->x,leafCube->bounds[1]->y, leafCube->bounds[1]->z); */
/* 		  panelIntersectsChildCube(c->tree->panels[c->panelindices[0]+1], leafCube->parent, cx, cy, cz); */
/* 		  printf("trackCube set to %d!\n", trackCube); */
/* 		} */
/* 	 } */
/* 	 if (poo) { */
/* /\* 		printf("pointlength > 4.0 and somehow isinside = 1? panelindex = %d, np = %d\n", c->panelindices[0], *\/ */
/* /\* 				 c->numpanelindices); *\/ */
/* /\* 		printPanel(c->tree->panels[c->panelindices[0]]); *\/ */

/* /\* 		printf("point (%f,%f,%f) should be within\n     bounds = (%f,%f,%f) - (%f,%f,%f)\n", *\/ */
/* /\* 				 point->x, point->y, point->z, *\/ */
/* /\* 				 c->bounds[0]->x,c->bounds[0]->y,c->bounds[0]->z, *\/ */
/* /\* 				 c->bounds[1]->x,c->bounds[1]->y,c->bounds[1]->z); *\/ */
/* 	 } */
	 
  }
  Vector3D_copy(closestPoint, c->tree->panels[c->panelindices[0]]->centroid);
  unsigned int *panelindicesPossIntersect;
  unsigned int numpanelindicesPossIntersect;
  //  findPossiblyIntersectingPanels(closestPoint, c, &numpanelindicesPossIntersect, &panelindicesPossIntersect);
  c->numpanelindices = c->numpanelindices;
  c->panelindices = c->panelindices;
  for (i = 1; i < c->numpanelindices; i++) {
/* 	 if (doubleCheckSurface) { */
/* 		printf("checking %d < %d\n", i, c->numpanelindices); */
/* 	 } */
	 if (rayIntersectsTriangle(ray, point, c->tree->panels[c->panelindices[i]],testIntersection)) {
/* 		if (doubleCheckSurface) { */
/* 		  printf("the same ray intersects panel %d.\n",i); */
/* 		  printf("distance old: %f,  distance new: %f\n", (float)Vector3D_distance(point, closestPoint), */
/* 					(float)Vector3D_distance(point, testIntersection)); */
/* 		  Vector3D poo1 = Vector3D_allocate(); */
/* 		  Vector3D poo2 = Vector3D_allocate(); */
/* 		  Vector3D_sub(poo1, closestPoint, point); real r1 = Vector3D_length(poo1); Vector3D_normalize(poo1); */
/* 		  Vector3D_sub(poo2, testIntersection, point); real r2 = Vector3D_length(poo2); Vector3D_normalize(poo2); */
/* 		  printf("length old = %f, length new = %f\n", r1, r2); */
/* 		  printf("vec1 = (%f %f %f)\nvec2 = (%f %f %f)\n", poo1->x,poo1->y,poo1->z,poo2->x,poo2->y,poo2->z); */
/* 		  Vector3D_free(poo1); */
/* 		  Vector3D_free(poo2); */
/* 		} */
		if (Vector3D_distance(point, closestPoint)
			 > Vector3D_distance(point, testIntersection)) {
		  Vector3D_copy(closestPoint, testIntersection);
/* 		  if (doubleCheckSurface) { */
/* 			 printf("new intersection is closer--checking inside/outside...\n"); */
/* 		  } */
		  if (Vector3D_dot(ray, c->tree->panels[c->panelindices[i]]->normal) > 0.0) {
/* 			 if (doubleCheckSurface) { 	printf("normals are aligned by %f on %d and inside\n", (float)Vector3D_dot(ray,c->tree->panels[c->panelindices[i]]->normal), i); } */
			 isInside = 1;
		  } else {
/* 			 if (doubleCheckSurface) {	printf("normals are aligned by %f on %d and outside\n", (float)Vector3D_dot(ray,c->tree->panels[c->panelindices[i]]->normal),i); } */
			 isInside = 0;
		  }
		} else {
/* 		  if (doubleCheckSurface) { printf("new intersection is not closer. skipping.\n"); } */
		}
	 } else {
/* 		if (doubleCheckSurface) { */
/* 		  printf("ray does not intersect %d (panel %d)\n",i,c->panelindices[i]); */
/* 		} */
	 }
	 
  }

  // the below are taken out for static
/*   Vector3D_free(testIntersection); */
/*   Vector3D_free(closestPoint); */ 
/*   Vector3D_free(ray); */

  return isInside;
}

void recursivelyInitializeCubeWithScatterData(SurfaceOperator surfoper, Cube c, real density) {
  unsigned int ix, iy, iz;
  Cube cnew;
  if (c->leaf) {
    c->isInsideProtein = 0;
    c->densityInsideCube = 0;
    if (pointIsInsideSurface(surfoper, c->center)) {
      c->isInsideProtein = 1;
      c->densityInsideCube = density;
    }
  } else {
    for (ix = 0; ix < 2; ix++) {
      for (iy = 0; iy < 2; iy++) {
	for (iz = 0; iz < 2; iz++) {
	  cnew = c->children[ix][iy][iz];
	  recursivelyInitializeCubeWithScatterData(surfoper, cnew, density);
	}
      }
    }
  }
}

void initializeOnlyOneSurfOperTree(PBEproblem problem, SurfaceOperator surfoper, real densityInside)
{
  Cube c = surfoper->tree->root;
  recursivelyInitializeCubeWithScatterData(surfoper, c, densityInside);
}


int recurseToGetPointsStatus(PBEproblem problem, Vector3D point)
{
  Cube c = problem->pbesurfaceoperator->children[0]->children[0]->tree->root;
  if ((point->x < c->bounds[0]->x) || (point->x > c->bounds[1]->x))
	 return 0;
  if ((point->y < c->bounds[0]->y) || (point->y > c->bounds[1]->y))
	 return 0;
  if ((point->z < c->bounds[0]->z) || (point->z > c->bounds[1]->z))
	 return 0;

  printf("calling recursiveCall...\n");
  return recursiveCallForPointStatus(c, point);
}

int recursiveCallForPointStatus(Cube c, Vector3D point)
{
  if (c->leaf) {
	 printf("isInsideProtein = %u\n", c->isInsideProtein);
	 printf("cube xlimits = %f < %f < %f ?\n", c->bounds[0]->x, point->x, c->bounds[1]->x);
	 printf("cube ylimits = %f < %f < %f ?\n", c->bounds[0]->y, point->y, c->bounds[1]->y);
	 printf("cube zlimits = %f < %f < %f ?\n", c->bounds[0]->z, point->z, c->bounds[1]->z);
	 return c->isInsideProtein;
  }
  
  unsigned int ix, iy, iz;
  ix  = (point->x > c->center->x)?1:0;
  iy  = (point->y > c->center->y)?1:0;
  iz  = (point->z > c->center->z)?1:0;

  return recursiveCallForPointStatus(c->children[ix][iy][iz], point);
}


void initializeSurfOperatorTreesWithScatterData(PBEproblem problem, real proteinDensity, real hydrationDensity) {
  excludedVolumeDensity = proteinDensity;
  hydrationLayerDensity = hydrationDensity;
  printf("Entering initializeSurfOperatorTreesWithScatterData: protein density=%lf and hydration layer density=%lf\n", excludedVolumeDensity, hydrationLayerDensity);

  SurfaceOperator cursurfoper;
  unsigned int i,j,k,l;
  
  for (i = 0; i < problem->pbesurfaceoperator->numchildren; i++) {
    printf("numchildren = %d, pointer = %d\n", problem->pbesurfaceoperator->numchildren,0);
    
    cursurfoper = problem->pbesurfaceoperator->children[i];
    //cursurfoper->tree now holds the outer stern (or one of them, if there are multiple...)
    
    // initialize the associated surfaces
    printf("initializing outer stern with %d panels and density %f\n",cursurfoper->mynumpanels,
	   (float)hydrationLayerDensity);
    initializeOnlyOneSurfOperTree(problem, cursurfoper, hydrationLayerDensity);
    
    for (j = 0; j < problem->pbesurfaceoperator->children[i]->numchildren; j++) {
      cursurfoper = problem->pbesurfaceoperator->children[i]->children[j];
      //cursurfoper->tree now holds one of the outer dielectric surfaces
      
      // initialize the associated outer dielectric surfaces
      printf("initializing dielectric with %d panels and density %f\n",cursurfoper->mynumpanels, (float)(excludedVolumeDensity));
      initializeOnlyOneSurfOperTree(problem,cursurfoper,excludedVolumeDensity);
      
      for (k = 0; k < cursurfoper->numchildren; k++) {
	cursurfoper = problem->pbesurfaceoperator->children[i]->children[j]->children[k];
	//cursurfoper->tree now holds one of the cavities

	// initialize the associated dielectric cavity surfaces
	printf("initializing dielectric cavity with %d panels\n",cursurfoper->mynumpanels);
	initializeOnlyOneSurfOperTree(problem, cursurfoper, hydrationLayerDensity);
	
	for (l = 0; l < cursurfoper->numchildren; l++) {
	  cursurfoper = problem->pbesurfaceoperator->children[i]->children[j]->children[k]->children[l];
	  //cursurfoper->tree now holds one of the stern cavities
	  
	  // initialize that stern cavity
	  printf("initializing stern cavity with %d panels\n",cursurfoper->mynumpanels);
	  initializeOnlyOneSurfOperTree(problem, cursurfoper, excludedVolumeDensity);
	}
      }
    }
  }
  printf("Exiting initializeSurfOperatorTreesWithScatterData\n");
}


real evaluateMolecularScatteringVolume(PBEproblem problem) {
  return scatterEvalVolume(problem->pbesurfaceoperator->children[0]->children[0]->tree->root);
}

// this doesn't _actually_ calculate volume, it calculates total
// _scattering_ volume, which is a meaningless quantity!
real scatterEvalVolume(Cube cube)
{
  real volume = 0.0;
  if (cube->leaf) {
	 Vector3D diff = Vector3D_allocate();
	 Vector3D_sub(diff, cube->bounds[1], cube->bounds[0]);
	 /* HERE*/ volume = cube->densityInsideCube * diff->x * diff->x * diff->x; /* HERE */
	 Vector3D_free(diff);
  } else {
	 unsigned int cx, cy, cz;
	 for (cx = 0; cx < 2; cx++) {
		for (cy = 0; cy < 2; cy++) {
		  for (cz = 0; cz < 2; cz++) {
			 if (cube->children[cx][cy][cz] != NULL) {
				volume += scatterEvalVolume(cube->children[cx][cy][cz]);
			 }
		  }
		}
	 }
  }
  return volume;
}

void scattercube_free(scattercube c)
{
  Vector3D_free(c->r);
  Vector3D_free(c->bounds[0]);
  Vector3D_free(c->bounds[1]);
  if (c->numpanelindices > 0)
	 free(c->panelindices);
  unsigned int i,j,k;
  for (i = 0; i < 2; i++) {
	 for (j = 0; j < 2; j++) {
		for (k = 0; k < 2; k++) {
		  c->children[i][j][k] = NULL;
		}
	 }
  }
  free(c);
}

scattercube scattercube_allocate(real size, Vector3D r, unsigned int level, scattercube parent)
{
  scattercube c = (scattercube)calloc(1, sizeof(_scattercube));
  c->size = size;
  c->r = Vector3D_allocate();
  c->bounds[0] = Vector3D_allocate();
  c->bounds[1] = Vector3D_allocate();
  c->parent = parent; // for now
  c->level = level;
  c->leaf = 1;
  c->panelindices = NULL;
  c->numpanelindices = 0;
  unsigned int i,j,k;

  for (i = 0; i < 2; i++) {
	 for (j = 0; j < 2; j++) {
		for (k = 0; k < 2; k++) {
		  c->children[i][j][k] = NULL;
		}
	 }
  }
  Vector3D_copy(c->r, r);
  return c;
}

complexreal evalTransformOfLeafCube(Cube c, Vector3D mu)
{
  complexreal F;
  real dx = c->bounds[1]->x - c->bounds[0]->x;
  real dy = c->bounds[1]->y - c->bounds[0]->y;
  real dz = c->bounds[1]->z - c->bounds[0]->z;
  real dxInt, dyInt, dzInt;

  complexreal transformCenter = cexp(-I * Vector3D_dot(c->center, mu)); // - matches the atomic contribution
  //  printf("mu = %f, %f, %f\n", mu->x, mu->y, mu->z);
  if (fabs((float)mu->x) > 1e-6) {
	 dxInt = 2. * sin(mu->x * dx/2.) / mu->x;
  } else {
	 dxInt = 2. * cos(mu->x * dx/2.) * (dx/2.);
  }
  if (fabs((float)mu->y) > 1e-6) {
	 dyInt = 2. * sin(mu->y * dy/2.) / mu->y;
  } else {
	 dyInt = 2. * cos(mu->y * dy/2.) * (dy/2.);
  }
  if (fabs((float)mu->z) > 1e-6) {
	 dzInt = 2. * sin(mu->z * dz/2.) / mu->z;
  } else {
/* 	 printf("dying!\n"); */
	 dzInt = 2. * cos(mu->z * dz/2.) * (dz/2.);
  }

/*   printf("%f %f %f %f %f %f %f %f %f %f\n", (float)creal(transformCenter), */
/* 			(float)cimag(transformCenter), */
/* 			(float)c->center->x, (float)c->center->y, (float)c->center->z, */
/* 			(float)dxInt, */
/* 			(float)dyInt, */
/* 			(float)dzInt, (float) mu->z, */
/* 			(float) -2* sin(mu->z * dz/2) / mu->z); */
  F =  transformCenter * dxInt * dyInt * dzInt;
  return F;
}

complexreal evalTransformCubeRecursively(Cube c, Vector3D mu)
{
  complexreal F_mu_cube = 0.0;
  if (c->leaf) {
	 if (c->isInsideProtein > 0) {
		F_mu_cube = evalTransformOfLeafCube(c, mu);
	 }
  } else {
	 unsigned int ix, iy, iz;
	 Cube cnew;
	 for (ix = 0; ix < 2; ix++) {
		for (iy = 0; iy < 2; iy++) {
		  for (iz = 0; iz < 2; iz++) {
			 cnew = c->children[ix][iy][iz];
			 F_mu_cube += evalTransformCubeRecursively(cnew, mu);
		  }
		}
	 }
  }
  return F_mu_cube;
}

complexreal evalTransformOneSurface(SurfaceOperator surfoper, Vector3D mu)
{
  Cube c = surfoper->tree->root;
  complexreal F_mu_surface = 0.0;

  F_mu_surface = evalTransformCubeRecursively(c, mu);
  return F_mu_surface;
}

void evalTransform(ScatterOptions options, PBEproblem problem, ScatterParameters parameters, Vector3D mu, complexreal *excludedVolume, complexreal *hydrationLayer)
{
  complexreal F_mu = 0.0;
  *excludedVolume = 0.0;
  *hydrationLayer = 0.0;
  SurfaceOperator cursurfoper;
  unsigned int i,j,k,l;
  
  for (i = 0; i < problem->pbesurfaceoperator->numchildren; i++) {
	 cursurfoper = problem->pbesurfaceoperator->children[i];
	 //cursurfoper->tree now holds the outer stern

	 F_mu = evalTransformOneSurface(cursurfoper, mu);
	 *hydrationLayer += hydrationLayerDensity * F_mu;
	 
	 for (j = 0; j < problem->pbesurfaceoperator->children[i]->numchildren; j++) {
		cursurfoper = problem->pbesurfaceoperator->children[i]->children[j];
		//cursurfoper->tree now holds one of the outer dielectric surfaces
		
		F_mu = evalTransformOneSurface(cursurfoper, mu);
 		*hydrationLayer -= hydrationLayerDensity * F_mu;
		*excludedVolume -= excludedVolumeDensity * F_mu;

		for (k = 0; k < problem->pbesurfaceoperator->children[i]->children[j]->numchildren; k++) {
		  cursurfoper = problem->pbesurfaceoperator->children[i]->children[j]->children[k];
		  //cursurfoper->tree now holds one of the cavities
		  
		  F_mu = evalTransformOneSurface(cursurfoper, mu);
		  *hydrationLayer += hydrationLayerDensity * F_mu;
		  *excludedVolume += excludedVolumeDensity * F_mu;

		  for (l = 0; l < problem->pbesurfaceoperator->children[i]->children[j]->children[k]->numchildren; l++) {
			 cursurfoper = problem->pbesurfaceoperator->children[i]->children[j]->children[k]->children[l];
			 //cursurfoper->tree now holds one of the stern cavities
			 
			 F_mu = evalTransformOneSurface(cursurfoper, mu);
			 *hydrationLayer -= hydrationLayerDensity * F_mu;
			 //			 *excludedVolume -= excludedVolumeDensity * F_mu;
		  }
		  
		}
		
	 }

  }
  
}

/* void scatterEvalInsideOutsideCubesSphere(PBEproblem problem, Cube cube) */
/* { */
/*   if (cube->leaf) { */
/* 	 Vector3D tmp = Vector3D_allocate(); */
/* 	 unsigned int i; */
/* 	 for (i = 0; i < problem->numpdbentries; i++) { */
/* 		tmp->x = problem->pdbentries[i].x; */
/* 		tmp->y = problem->pdbentries[i].y; */
/* 		tmp->z = problem->pdbentries[i].z; */
/* 		if (Vector3D_distance(cube->center, tmp) < problem->pdbentries[i].radius) { */
/* 		  cube->isInsideProtein = 1; */
/* 		  cube->densityInsideCube = 1; */
/* 		  break; */
/* 		} */
/* 		// handle reentrant surface... how?  don't know yet.  for right now, it's  */
/* 	 } */
/* 	 Vector3D_free(tmp); */
/*   } else { */
/* 	 unsigned int cx, cy, cz; */
/* 	 for (cx = 0; cx < 2; cx++) { */
/* 		for (cy = 0; cy < 2; cy++) { */
/* 		  for (cz = 0; cz < 2; cz++) { */
/* 			 if (cube->children[cx][cy][cz] != NULL) { */
/* 				scatterEvalInsideOutsideCubes(problem, cube->children[cx][cy][cz]); */
/* 			 } */
/* 		  } */
/* 		} */
/* 	 } */
/*   } */
/* } */

/* // this function actually does the spherical averaging */
void calculateScatteringAtParticularAngle(ScatterOptions options, PBEproblem problem, ScatterParameters parameters,
					  real k, real *Ik, real *IkFMS, real *debyeK,
					  real *exclK, real *borderK, real *FMS)
{
  complexreal continuumExcludedPerturbation,continuumHydrationPerturbation;
  complexreal explicitAtomContribution;
  complexreal totalScatteringAtKVector;
  complexreal FMSScatteringAtKVector;
  complexreal totalFMSScatteringAtKVector;
  Vector3D currentQuadPointInKSpace = Vector3D_allocate();
  unsigned int i;
  *Ik = 0.0;
  *debyeK = 0.0;
  *exclK = 0.0;
  *borderK = 0.0;

  for (i = 0; i < parameters->numSphQuadPoints; i++) { 
	 Vector3D_copy(currentQuadPointInKSpace, parameters->sphQuadPoints[i]);
	 Vector3D_scale(currentQuadPointInKSpace, k);

	 continuumExcludedPerturbation = 0.0;
	 continuumHydrationPerturbation = 0.0;

	 evalTransform(options, problem, parameters, currentQuadPointInKSpace, &continuumExcludedPerturbation, &continuumHydrationPerturbation);

	 explicitAtomContribution = 0.0;
	 FMSScatteringAtKVector = 0.0;
 	 evalAtomicScattering(options, problem, parameters, currentQuadPointInKSpace, excludedVolumeDensity, &explicitAtomContribution, &FMSScatteringAtKVector);

	 totalFMSScatteringAtKVector = explicitAtomContribution + FMSScatteringAtKVector;
	 totalScatteringAtKVector = explicitAtomContribution + continuumExcludedPerturbation + continuumHydrationPerturbation;
	 /* printf("currentpoint = %f, %f, %f\n", currentQuadPointInKSpace->x,currentQuadPointInKSpace->y,currentQuadPointInKSpace->z); */
	 /* printf("continuumExcluded = %f\nhydrationLayer = %f\nexplicitAtom = %f\n", cabs(continuumExcludedPerturbation), cabs(continuumHydrationPerturbation), cabs(explicitAtomContribution)); */
	 /* exit(-1); */
	 *Ik      += parameters->sphQuadWeights[i] * pow(cabs(totalScatteringAtKVector),2.0);
	 *debyeK  += parameters->sphQuadWeights[i] * pow(cabs(explicitAtomContribution),2.0);
	 *exclK   += parameters->sphQuadWeights[i] * pow(cabs(continuumExcludedPerturbation),2.0);
	 *borderK += parameters->sphQuadWeights[i] * pow(cabs(continuumHydrationPerturbation), 2.0);
	 *IkFMS   += parameters->sphQuadWeights[i] * pow(cabs(totalFMSScatteringAtKVector),2.0);
	 *FMS     += parameters->sphQuadWeights[i] * pow(cabs(FMSScatteringAtKVector),2.0);
  }
  Vector3D_free(currentQuadPointInKSpace);

}

void calculateScatteringPattern(ScatterOptions options,
				PBEproblem problem,
				ScatterParameters parameters,
				ScatterIntensities intensities) {
  unsigned int i, j;
  for (i = 0; i < options->numQpoints; i++) {
    calculateScatteringAtParticularAngle(options, problem, parameters, intensities->sAngInv[i],
					 &(intensities->IsAngInv[i]), &(intensities->IsFMSAngInv[i]),
					 &(intensities->DebyeAngInv[i]), &(intensities->ExclAngInv[i]),
					 &(intensities->BorderAngInv[i]),&(intensities->FMSExcl[i]));
  }
}


//// BELOW HERE IS JUST GARBAGE I HAVEN"T TAKEN OUT YET

void scatterEvalInsideOutsideCubes(PBEproblem problem, Cube cube)
{
  printf("this function should not be called anymore!!\n");
  exit(-1);
  real excludedVolumeDensity = 1.0;
  real hydrationLayerDensity = 1.0;

  if (cube->leaf) {
	 Vector3D tmp = Vector3D_allocate();
	 unsigned int i;

	 // this is commented out for the time being while we test the "is cube inside or outside the surfaces"
/* 	 for (i = 0; i < problem->numpdbentries; i++) { */
/* 		tmp->x = problem->pdbentries[i].x; */
/* 		tmp->y = problem->pdbentries[i].y; */
/* 		tmp->z = problem->pdbentries[i].z; */
/* 		//		printf("problem->pdbentries[%d].radius = %f compare %f\n", i, (float) problem->pdbentries[i].radius,Vector3D_distance(cube->center, tmp) ); */
/* 		if (Vector3D_distance(cube->center, tmp) < problem->pdbentries[i].radius) { */
/* 		  cube->isInsideProtein = 1; */
/* 		  cube->densityInsideCube = excludedVolumeDensity; */
/* 		  printf("(%f,%f,%f) (%f): inside protein spheres\n",(float)cube->center->x,(float)cube->center->y, */
/* 					(float)cube->center->z, (float)Vector3D_length(cube->center)); */
/* 		  Vector3D_free(tmp); */
/* 		  return; */
/* 		} */
/* 		// handle reentrant surface... how?  don't know yet.  for right now, it's  */
/* 	 } */

	 // end the "this is commented out..." bit
	 
	 scatterDensity density = BULK_SOLVENT;
	 cube->isInsideProtein = 0;
	 cube->densityInsideCube = 0;
	 switch(density) {
	 case HYDRATION_LAYER:
	   cube->isInsideProtein = 0;
	   cube->densityInsideCube = hydrationLayerDensity;
	   break;
	 case PROTEIN_INTERIOR:
	   cube->isInsideProtein = 1;
	   cube->densityInsideCube = excludedVolumeDensity;
	   break;
	 case BULK_SOLVENT:
	   cube->isInsideProtein = 0;
	   cube->densityInsideCube = 0;
	   break;
	 }
	 Vector3D_free(tmp);
  } else {
	 unsigned int cx, cy, cz;
	 for (cx = 0; cx < 2; cx++) {
		for (cy = 0; cy < 2; cy++) {
		  for (cz = 0; cz < 2; cz++) {
			 if (cube->children[cx][cy][cz] != NULL) {
				scatterEvalInsideOutsideCubes(problem, cube->children[cx][cy][cz]);
			 }
		  }
		}
	 }
  }
}


// this is NOT OPTIMIZED.  in fact it's probably as slow as slow can be.
scatterDensity figureOutWhichRegionContainsPoint(PBEproblem problem, Vector3D point) {
/*   printf("checking point (%f,%f,%f):(%f)...",(float)point->x,(float)point->y,(float)point->z,(float)Vector3D_length(point)); */
  SurfaceOperator cursurfoper1, cursurfoper2, cursurfoper3, cursurfoper4;
  unsigned int i,j,k,l;

  for (i = 0; i < problem->pbesurfaceoperator->numchildren; i++) {
	 cursurfoper1 = problem->pbesurfaceoperator->children[i];
	 //cursurfoper1->tree now holds the outer stern
	 
	 for (j = 0; j < problem->pbesurfaceoperator->children[i]->numchildren; j++) {
		cursurfoper2 = problem->pbesurfaceoperator->children[i]->children[j];
		//cursurfoper2->tree now holds one of the outer dielectric surfaces
		
		for (k = 0; k < problem->pbesurfaceoperator->children[i]->children[j]->numchildren; k++) {
		  cursurfoper3 = problem->pbesurfaceoperator->children[i]->children[j]->children[k];
		  //cursurfoper3->tree now holds one of the cavities

		  for (l = 0; l < problem->pbesurfaceoperator->children[i]->children[j]->children[k]->numchildren; l++) {
			 cursurfoper4 = problem->pbesurfaceoperator->children[i]->children[j]->children[k]->children[l];
			 //cursurfoper4->tree now holds one of the stern cavities

			 if (pointIsInsideSurface(cursurfoper4, point)) {
/* 				printf("case 1.\n"); */
				return BULK_SOLVENT;
			 }
		  }

		  if (pointIsInsideSurface(cursurfoper3, point)) {
/* 			 printf("case 2.\n"); */
			 return HYDRATION_LAYER;
		  }
		}

		if (pointIsInsideSurface(cursurfoper2, point)) {
/* 		  printf("case 3.\n"); */
		  return PROTEIN_INTERIOR;
		}
	 }

	 if (pointIsInsideSurface(cursurfoper1, point)) {
/* 		printf("case 4.\n"); */
		return HYDRATION_LAYER;
	 }
  }

  return BULK_SOLVENT;
}

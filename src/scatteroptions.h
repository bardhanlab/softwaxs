#ifndef __SCATTEROPTIONS_H__
#define __SCATTEROPTIONS_H__

typedef struct _ScatterOptions {
  char *PDB;
  char *SRF;
  char *SIZ;
  char *CRY;
  char *parameterDirectory;
  char *outputFile;
  char *paramFile;
  char *anomalousFile;
  unsigned int anomalousAtomicNumber;
  unsigned int resolution;
  unsigned int useBfactors;
  real Rm_scale;

  unsigned int numSphQuad;
  char         *sphQuadFile;
  char         *lowAfile,  *lowBfile;
  char         *medAfile,  *medBfile;
  char         *highAfile, *highBfile;

  real minQ, maxQ;
  unsigned int numQpoints;
  real excludedVolumeDensity;
  real hydrationShellDensity;
  
  unsigned int useMADMAX;
  unsigned int energy;
} _ScatterOptions;

typedef _ScatterOptions* ScatterOptions;
ScatterOptions ScatterOptions_allocate();
void ScatterOptions_free(ScatterOptions options);
void ScatterOptions_setDefaults(ScatterOptions options);
void ScatterOptions_parseArgs(ScatterOptions options, int argc, char** argv);
void ScatterOptions_loadConfigFile(ScatterOptions options, char *configFile);
#endif

#include "FFTSVD.h"

/* Constructors and Destructors */

int edgeIntersectsTriangle(Vector3D v1, Vector3D v2, Vector3D normal, Vector3D *triangle)
{
  static int initialized = 0;
  static Vector3D u, v, w0, w, dir, testIntersection;
  if (! initialized) {
	 u = Vector3D_allocate();
	 v = Vector3D_allocate();
	 w0 = Vector3D_allocate();
	 w = Vector3D_allocate();
	 dir = Vector3D_allocate();
	 testIntersection = Vector3D_allocate();
	 initialized = 1;
  }

  real r, a, b;
  Vector3D_sub(u, triangle[1], triangle[0]);
  Vector3D_sub(v, triangle[2], triangle[0]);
  Vector3D_sub(w0, v1, triangle[0]);
  Vector3D_sub(dir, v2, v1);
  a = -Vector3D_dot(normal, w0);
  b = Vector3D_dot(normal, dir);

  r = a/b;

  if ((r < 0.0) || (r > 1.0)) {
/* 	 Vector3D_free(u); */
/* 	 Vector3D_free(v); */
/* 	 Vector3D_free(w0); */
/* 	 Vector3D_free(w); */
/* 	 Vector3D_free(dir); */
	 return 0;
  }
  Vector3D_addscaled(testIntersection, v1, r, dir);

/*   if (trackCube) { */
/* 	 printf("r=%f    testInt = %f %f %f\n",r, testIntersection->x, testIntersection->y, testIntersection->z); */
/*   } */
  
  real uu, uv, vv, wu, wv, D;
  uu = Vector3D_dot(u,u);
  uv = Vector3D_dot(u,v);
  vv = Vector3D_dot(v,v);
  Vector3D_sub(w, testIntersection, triangle[0]);
  wu = Vector3D_dot(w,u);
  wv = Vector3D_dot(w,v);
  D = uv * uv - uu * vv;

  real s, t;
  s = (uv * wv - vv * wu) / D;
  if ((s < 0.0) || (s > 1.0)) {
/* 	 Vector3D_free(u); */
/* 	 Vector3D_free(v); */
/* 	 Vector3D_free(w0); */
/* 	 Vector3D_free(w); */
/* 	 Vector3D_free(testIntersection); */
/* 	 Vector3D_free(dir); */
	 return 0;
  }
  t = (uv * wu - uu * wv) / D;
  if ((t < 0.0) || ((s + t) > 1.0)) {
/* 	 Vector3D_free(u); */
/* 	 Vector3D_free(v); */
/* 	 Vector3D_free(w0); */
/* 	 Vector3D_free(w); */
/* 	 Vector3D_free(testIntersection); */
/* 	 Vector3D_free(dir); */
	 return 0;
  }
/*   if (trackCube) { */
/* 	 printf("v1 = %f, %f, %f\n", v1->x, v1->y, v1->z); */
/* 	 printf("v2 = %f, %f, %f\n", v2->x, v2->y, v2->z); */
/*   } */

/*   Vector3D_free(w); */
/*   Vector3D_free(w0); */
/*   Vector3D_free(v); */
/*   Vector3D_free(u); */
/*   Vector3D_free(testIntersection); */
/*   Vector3D_free(dir); */
  return 1;

}

int edgeIntersectsSquare(Vector3D v2, Vector3D v1, 
								 Vector3D s1, Vector3D s2, Vector3D s3, Vector3D s4)
{

  static int initialized = 0;
  static Vector3D triangle1[3];
  static Vector3D triangle2[3];
  static Vector3D normal, d1, d2;
  unsigned int i;
  int doesIntersect = 0;

  if (! initialized) {
	 for (i = 0; i < 3; i++) {
		triangle1[i] = Vector3D_allocate();
		triangle2[i] = Vector3D_allocate();
	 }
	 normal = Vector3D_allocate();
	 d1 = Vector3D_allocate();
	 d2 = Vector3D_allocate();
	 initialized = 1;
  }
  
  Vector3D_copy(triangle1[0], s1);
  Vector3D_copy(triangle1[1], s2);
  Vector3D_copy(triangle1[2], s3);

  Vector3D_copy(triangle2[0], s1);
  Vector3D_copy(triangle2[1], s3);
  Vector3D_copy(triangle2[2], s4);

  Vector3D_sub(d1, s2, s1);
  Vector3D_sub(d2, s3, s1);

  Vector3D_cross(normal, d1, d2); Vector3D_normalize(normal);
  
  // assuming clockwise but it don't matter to jesus
  if (edgeIntersectsTriangle(v1, v2, normal, triangle1)
		|| edgeIntersectsTriangle(v1, v2, normal, triangle2)) {
	 doesIntersect = 1;
  } else {
	 doesIntersect = 0;
  }

/*   for (i = 0; i < 3; i++) { */
/* 	 Vector3D_free(triangle1[i]); */
/* 	 Vector3D_free(triangle2[i]); */
/*   } */
/*   Vector3D_free(normal); */
/*   Vector3D_free(d1); */
/*   Vector3D_free(d2); */

  return doesIntersect;
/*   Vector3D normal = Vector3D_allocate(); */
/*   Vector3D_copy(normal, inplane); */
/*   real scaleFactor = 1.0; */
/*   if (inplane == min) */
/* 	 scaleFactor = -1.0; */
/*   Vector3D_scale(normal, scaleFactor); */
/*   real junk1 = -(Vector3D_dot(normal, inplane) - Vector3D_dot(normal, v2)); */
/*   real junk2 = Vector3D_dot(normal, edgeVector); */
/*   Vector3D_free(normal); */
/*   if (fabs((float)junk2) < 1e-8) {  */
/* 	 return 0; */
/*   } */
/*   if (! ((junk1/junk2 >= 0) & (junk1/junk2 <= 1))) { */
/* 	 return 0; */
	 
/*   } */
/*   real s = junk1/junk2; */
/*   Vector3D_addscaled(edgeVector, v1, s, edgeVector); */
/*   if ((face->x > 0)  */
/* 		&& ((edgeVector->y > min->y) && (edgeVector->y < max->y)) */
/* 		&& ((edgeVector->z > min->z) && (edgeVector->z < max->z)) ){ */
/* 	 doesIntersect = 1; */
/*   } else if ((face->y > 0)  */
/* 				 && ((edgeVector->x > min->x) && (edgeVector->x < max->x))  */
/* 				 && ((edgeVector->z > min->z) && (edgeVector->z < max->z)) ){ */
/* 	 doesIntersect = 1; */
/*   } else if ((face->z > 0) */
/* 				 && ((edgeVector->y > min->y) && (edgeVector->y < max->y)) */
/* 				 && ((edgeVector->x > min->x) && (edgeVector->x < max->x)) ){ */
/* 	 doesIntersect = 1; */
/*   } */

/*   return doesIntersect; */

  
}

int panelIntersectsChildCube(Panel panel, Cube parent, unsigned int cx, unsigned int cy, unsigned int cz) {

  static int initialized = 0;
  unsigned int i;
  FlatPanel fp = (FlatPanel)panel->realpanel;
  int doesIntersect = 0;
  static Vector3D delta, childcenter, diff, childmin, childmax, X, Y, Z, edge, v[8];
  if (! initialized) {
	 delta = Vector3D_allocate();
	 childcenter = Vector3D_allocate();
	 diff = Vector3D_allocate();
	 childmin = Vector3D_allocate();
	 childmax = Vector3D_allocate();
	 edge = Vector3D_allocate();
	 X = Vector3D_allocate(); X->x = 1.0; X->y = 0.0; X->z = 0.0;
	 Y = Vector3D_allocate(); Y->x = 0.0; Y->y = 1.0; Y->z = 0.0;
	 Z = Vector3D_allocate(); Z->x = 0.0; Z->y = 0.0; Z->z = 1.0;
	 for (i = 0; i < 8; i++) {
		v[i] = Vector3D_allocate();
	 }
	 initialized = 1;
  }
  Vector3D_sub(delta, parent->bounds[1],parent->bounds[0]);
  real parentEdgeLength = delta->x;
  real childEdgeLength = 0.5 * parentEdgeLength;

  Vector3D_copy(childcenter, parent->center);
  childcenter->x += ((cx > 0)?+1:-1) * childEdgeLength / 2;
  childcenter->y += ((cy > 0)?+1:-1) * childEdgeLength / 2;
  childcenter->z += ((cz > 0)?+1:-1) * childEdgeLength / 2;

  // first check whether encompassing sphere can't intersect plane of triangle
  Vector3D_sub(diff, childcenter, fp->vertex[0]);
  real distanceToPlane = Vector3D_dot(diff, panel->normal);
  if (distanceToPlane*distanceToPlane > 3 * childEdgeLength * childEdgeLength) {
/* 	 Vector3D_free(diff); */
/* 	 Vector3D_free(childcenter); */
	 doesIntersect = 0;
	 return doesIntersect;
  }
  
  // then check whether any vertex is inside.  if so, boom!
  childmin->x = childcenter->x - childEdgeLength/2;
  childmin->y = childcenter->y - childEdgeLength/2;
  childmin->z = childcenter->z - childEdgeLength/2;
  childmax->x = childcenter->x + childEdgeLength/2;
  childmax->y = childcenter->y + childEdgeLength/2;
  childmax->z = childcenter->z + childEdgeLength/2;
/*   if (trackCube) { */
/*   printf("childcube (%d,%d,%d):\n", cx, cy, cz); */
/*   printf("min: %f %f %f\n", childmin->x,childmin->y, childmin->z); */
/*   printf("max: %f %f %f\n", childmax->x,childmax->y, childmax->z); */
/*   } */
  for (i = 0; i < 3; i++) {
	 if ((fp->vertex[i]->x > childmin->x) &&
		  (fp->vertex[i]->x < childmax->x) &&
		  (fp->vertex[i]->y > childmin->y) &&
		  (fp->vertex[i]->y < childmax->y) &&
		  (fp->vertex[i]->z > childmin->z) &&
		  (fp->vertex[i]->z < childmax->z)) {
		doesIntersect = 1;
/* 		Vector3D_free(diff); */
/* 		Vector3D_free(childmin); */
/* 		Vector3D_free(childmax); */
/* 		Vector3D_free(childcenter); */

/* 		if (trackCube) { */
/* 		  printf("panel has a vertex inside the cube centered at %f %f %f!\n",childcenter->x,childcenter->y,childcenter->z); */
/* 		  printf("vertex = %f, %f, %f\n", fp->vertex[i]->x, fp->vertex[i]->y,fp->vertex[i]->z); */
/* 		  printf("min = %f, %f, %f\n", childmin->x, childmin->y,childmin->z); */
/* 		  printf("max = %f, %f, %f\n", childmax->x, childmax->y,childmax->z); */
/* 		} */
		return doesIntersect;
	 }
  }

  // check whether any triangle edge is intersecting one of the sides
  unsigned int next;
/*   for (i = 0; i < 3; i++) { */
/* 	 next = i + 1; */
/* 	 if (next > 2) { */
/* 		next = 0; */
/* 	 } */
/* 	 Vector3D_sub(edge, fp->vertex[next], fp->vertex[i]); */
/* 	 if (edgeIntersectsSquare(childmin, childmax, X, childmin, edge, fp->vertex[next], fp->vertex[i]) */
/* 		  || edgeIntersectsSquare(childmin, childmax, X, childmax, edge, fp->vertex[next], fp->vertex[i]) */
/* 		  || edgeIntersectsSquare(childmin, childmax, Y, childmin, edge, fp->vertex[next], fp->vertex[i]) */
/* 		  || edgeIntersectsSquare(childmin, childmax, Y, childmax, edge, fp->vertex[next], fp->vertex[i]) */
/* 		  || edgeIntersectsSquare(childmin, childmax, Z, childmin, edge, fp->vertex[next], fp->vertex[i]) */
/* 		  || edgeIntersectsSquare(childmin, childmax, Z, childmax, edge, fp->vertex[next], fp->vertex[i])) { */
/* 		doesIntersect += 4; */
/* 		printf("panel has an edge inside the cube!\n"); */
/* 		i = 3; // break */
/* 	 } */
/*   } */


  unsigned int index, ix, iy, iz;
  for (ix = 0; ix < 2; ix++) {
	 edge->x = ix * childEdgeLength;
	 for (iy = 0; iy < 2; iy++) {
		edge->y = iy * childEdgeLength;
		for (iz = 0; iz < 2; iz++) {
		  edge->z = iz * childEdgeLength;
		  index = ix + iy*2  + iz*4;
		  Vector3D_copy(v[index], childmin);
		  Vector3D_add(v[index], edge, v[index]);
/* 		  if (trackCube) { */
/* 			 printf("v[%d] = %f %f %f\n", index, v[index]->x, v[index]->y, v[index]->z); */
/* 		  } */
		}
	 }
  }

  for (i = 0; i < 3; i++) {
	 next = (i+1) % 3;
	 if (   edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[0], v[1], v[3], v[2])
		  || edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[4], v[5], v[7], v[6])
		  || edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[0], v[1], v[5], v[4])
		  || edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[1], v[3], v[7], v[5])
		  || edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[3], v[2], v[6], v[7])
		  || edgeIntersectsSquare(fp->vertex[next],fp->vertex[i], v[2], v[0], v[4], v[6])) {
/* 		if (trackCube) {  */
/* 		  printf("triangle edge %d intersects the panel face!\n", i); */
/* 		  printf("edge v1 = %f, %f, %f\nedge v2 = %f, %f, %f\n", fp->vertex[i]->x, fp->vertex[i]->y, fp->vertex[i]->z, fp->vertex[next]->x, fp->vertex[next]->y, fp->vertex[next]->z); */
/* 		} */
		doesIntersect += 4;
	 }
  }

/*   if (trackCube) { */
/* 	 printf("vertex0 = %f, %f, %f\n", fp->vertex[0]->x, fp->vertex[0]->y, fp->vertex[0]->z); */
/* 	 printf("vertex1 = %f, %f, %f\n", fp->vertex[1]->x, fp->vertex[1]->y, fp->vertex[1]->z); */
/* 	 printf("vertex2 = %f, %f, %f\n", fp->vertex[2]->x, fp->vertex[2]->y, fp->vertex[2]->z); */
/* 	 printf("normal = %f, %f, %f\n", panel->normal->x, panel->normal->y, panel->normal->z); */
/* 	 printf("panelaxis = %f, %f, %f\n", fp->panelaxis[2]->x, fp->panelaxis[2]->y, fp->panelaxis[2]->z); */
/*   } */
  // check whether any childcube edge intersects the triangle
  if (   edgeIntersectsTriangle(v[0], v[1], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[1], v[3], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[3], v[2], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[2], v[0], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[0], v[4], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[1], v[5], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[2], v[6], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[3], v[7], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[4], v[5], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[5], v[7], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[7], v[6], panel->normal, fp->vertex)
		|| edgeIntersectsTriangle(v[6], v[4], panel->normal, fp->vertex) ) {
	 doesIntersect += 2;
  }
/*   for (i = 0; i < 8; i++) { */
/* 	 Vector3D_free(v[i]); */
/*   } */

/*   if (trackCube) { */
/*   if (doesIntersect == 2) { */
/* 	 printf("cube just intersects the panel in its middle and nowhere else!\n"); */
/*   } */
/*   } */
/*   free(v); */
/*   Vector3D_free(X); */
/*   Vector3D_free(Y); */
/*   Vector3D_free(Z); */
/*   Vector3D_free(edge); */
/*   Vector3D_free(diff); */
/*   Vector3D_free(childmin); */
/*   Vector3D_free(childmax); */
/*   Vector3D_free(childcenter); */
  return (doesIntersect > 0);
}

Cube Cube_allocate(Panel* panels, unsigned int* panelindices, unsigned int numpanelindices, Vector3D* points, unsigned int* pointindices, unsigned int numpointindices, unsigned int level, unsigned int indices[3], Vector3D bounds[2], Cube parent, Tree tree, unsigned int partitioningdepth) {
   Cube cube = (Cube)calloc(1, sizeof(_Cube));
   unsigned int i;
   unsigned int cx, cy, cz;

   cube->level = level;
   cube->indices[0] = indices[0];
   cube->indices[1] = indices[1];
   cube->indices[2] = indices[2];
   cube->bounds[0] = Vector3D_allocate();
   cube->bounds[1] = Vector3D_allocate();
   Vector3D_copy(cube->bounds[0], bounds[0]);
   Vector3D_copy(cube->bounds[1], bounds[1]);
   cube->center = Vector3D_allocate();
   Vector3D_add(cube->center, cube->bounds[0], cube->bounds[1]);
   Vector3D_scale(cube->center, 0.5);

   cube->isInsideProtein = 0;
   cube->densityInsideCube = 0.;
	
   cube->panelindices = (unsigned int*)calloc(numpanelindices, sizeof(unsigned int));
   for (i = 0; i < numpanelindices; i++)
      cube->panelindices[i] = panelindices[i];
   cube->numpanelindices = numpanelindices;

   cube->pointindices = (unsigned int*)calloc(numpointindices, sizeof(unsigned int));
   for (i = 0; i < numpointindices; i++)
      cube->pointindices[i] = pointindices[i];
   cube->numpointindices = numpointindices;

   cube->parent = parent;
   cube->tree = tree;


   if (cube->level == (partitioningdepth-1)) {
      cube->leaf = 1;
      return cube;
   }

   /* Allocate the children */

   for (cx = 0; cx <= 1; cx++)
      for (cy = 0; cy <= 1; cy++)
         for (cz = 0; cz <= 1; cz++) {
            unsigned int* childpanelindices;
            unsigned int numchildpanelindices = 0;
            unsigned int* childpointindices;
            unsigned int numchildpointindices = 0;
            unsigned int childindices[3];
            Vector3D childbounds[2];
            unsigned int count = 0;
            
            for (i = 0; i < numpanelindices; i++) {
	      if (panelIntersectsChildCube(panels[panelindices[i]], cube, cx, cy, cz)) 
		numchildpanelindices++;
            }

            for (i = 0; i < numpointindices; i++) {
               if ((((cx == 0) && (points[pointindices[i]]->x < cube->center->x)) ||
                    ((cx == 1) && (points[pointindices[i]]->x >= cube->center->x))) &&
                   (((cy == 0) && (points[pointindices[i]]->y < cube->center->y)) ||
                    ((cy == 1) && (points[pointindices[i]]->y >= cube->center->y))) &&
                   (((cz == 0) && (points[pointindices[i]]->z < cube->center->z)) ||
                    ((cz == 1) && (points[pointindices[i]]->z >= cube->center->z))))
                  numchildpointindices++;
            }

	    int shouldBeLeaf = 0;
	    if (numchildpanelindices == 0) {
	      //				  printf("level = %d cube should be leaf\n", cube->level+1);
	      shouldBeLeaf = 1;
	    }

            childpanelindices = (unsigned int*)calloc(numchildpanelindices, sizeof(unsigned int));

            count = 0;

            for (i = 0; i < numpanelindices; i++) {
	      if (panelIntersectsChildCube(panels[panelindices[i]], cube, cx, cy, cz)) {
		childpanelindices[count] = panelindices[i];
		count++;
	      }
            }

            childpointindices = (unsigned int*)calloc(numchildpointindices, sizeof(unsigned int));

            count = 0;

            for (i = 0; i < numpointindices; i++) {
               if ((((cx == 0) && (points[pointindices[i]]->x < cube->center->x)) ||
                    ((cx == 1) && (points[pointindices[i]]->x >= cube->center->x))) &&
                   (((cy == 0) && (points[pointindices[i]]->y < cube->center->y)) ||
                    ((cy == 1) && (points[pointindices[i]]->y >= cube->center->y))) &&
                   (((cz == 0) && (points[pointindices[i]]->z < cube->center->z)) ||
                    ((cz == 1) && (points[pointindices[i]]->z >= cube->center->z)))) {
                  childpointindices[count] = pointindices[i];
                  count++;
               }
            }
            
            childbounds[0] = Vector3D_allocate();
            childbounds[1] = Vector3D_allocate();
            
            Vector3D_copy(childbounds[0], cube->bounds[0]);
            Vector3D_copy(childbounds[1], cube->center);
            
            if (cx == 1) {
               real diff = cube->center->x - childbounds[0]->x;
               childbounds[0]->x += diff;
               childbounds[1]->x += diff;
            }
            if (cy == 1) {
               real diff = cube->center->y - childbounds[0]->y;
               childbounds[0]->y += diff;
               childbounds[1]->y += diff;
            }
            if (cz == 1) {
               real diff = cube->center->z - childbounds[0]->z;
               childbounds[0]->z += diff;
               childbounds[1]->z += diff;
            }
            
            childindices[0] = 2 * indices[0]; 
            childindices[1] = 2 * indices[1];
            childindices[2] = 2 * indices[2];
            
            if (cx == 1)
               childindices[0]++;
            if (cy == 1)
               childindices[1]++;
            if (cz == 1)
               childindices[2]++;

	    if (shouldBeLeaf) {
	      cube->children[cx][cy][cz] = Cube_allocate(panels, childpanelindices, numchildpanelindices, points, childpointindices, numchildpointindices, partitioningdepth-1, childindices, childbounds, cube, tree, partitioningdepth);
	    } else {
	      cube->children[cx][cy][cz] = Cube_allocate(panels, childpanelindices, numchildpanelindices, points, childpointindices, numchildpointindices, level+1, childindices, childbounds, cube, tree, partitioningdepth);
	    }
	    

            free(childpanelindices);
            free(childpointindices);
            Vector3D_free(childbounds[0]);
            Vector3D_free(childbounds[1]);
         }

   return cube;
}

void Cube_free(Cube cube) {
   unsigned int i;

   if (cube != NULL) {
      Vector3D_free(cube->bounds[0]);
      Vector3D_free(cube->bounds[1]);
      Vector3D_free(cube->center);
      free(cube->panelindices);
      free(cube->pointindices);
      free(cube->localcubes);
      free(cube->interactingcubes);

      if (cube->leaf) {
      } else {
         Cube_free(cube->children[0][0][0]);
         Cube_free(cube->children[0][0][1]);
         Cube_free(cube->children[0][1][0]);
         Cube_free(cube->children[0][1][1]);
         Cube_free(cube->children[1][0][0]);
         Cube_free(cube->children[1][0][1]);
         Cube_free(cube->children[1][1][0]);
         Cube_free(cube->children[1][1][1]);
      }
   }

   free(cube);
}

/* Operations */

void Cube_lists(Cube cube) {
   unsigned int cx, cy, cz, i, count;

   /* Initialize */
   
   cube->numlocalcubes = 0;
   cube->localcubes = NULL;
   cube->numinteractingcubes = 0;
   cube->interactingcubes = NULL;

   /* Siblings are guaranteed to be local cubes */
   
   if (cube->parent != NULL)
      for (cx = 0; cx <= 1; cx++)
         for (cy = 0; cy <= 1; cy++)
            for (cz = 0; cz <= 1; cz++)
               if (cube->parent->children[cx][cy][cz] != NULL)
                  if (cube->parent->children[cx][cy][cz] != cube)
                     cube->numlocalcubes++;
   
   /* Other local cubes are children of the parent's local cubes that are
      within the LOCAL cutoff */
   
   if (cube->parent != NULL)
      for (i = 0; i < cube->parent->numlocalcubes; i++)
         for (cx = 0; cx <= 1; cx++)
            for (cy = 0; cy <= 1; cy++)
               for (cz = 0; cz <= 1; cz++)
                  if (cube->parent->localcubes[i]->children[cx][cy][cz] != NULL)
                     if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[0] - cube->indices[0]) <= LOCAL)
                        if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[1] - cube->indices[1]) <= LOCAL)
                           if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[2] - cube->indices[2]) <= LOCAL)
                              cube->numlocalcubes++;

   /* Allocate */

   if (cube->numlocalcubes > 0)
      cube->localcubes = (Cube*)calloc(cube->numlocalcubes, sizeof(Cube*));

   count = 0;

   /* Siblings are guaranteed to be local cubes */
   
   if (cube->parent != NULL)
      for (cx = 0; cx <= 1; cx++)
         for (cy = 0; cy <= 1; cy++)
            for (cz = 0; cz <= 1; cz++)
               if (cube->parent->children[cx][cy][cz] != NULL)
                  if (cube->parent->children[cx][cy][cz] != cube) {
                     cube->localcubes[count] = cube->parent->children[cx][cy][cz];
                     count++;
                  }
   
   /* Other local cubes are children of the parent's local cubes that are
      within the LOCAL cutoff */
   
   if (cube->parent != NULL)
      for (i = 0; i < cube->parent->numlocalcubes; i++)
         for (cx = 0; cx <= 1; cx++)
            for (cy = 0; cy <= 1; cy++)
               for (cz = 0; cz <= 1; cz++)
                  if (cube->parent->localcubes[i]->children[cx][cy][cz] != NULL)
                     if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[0] - cube->indices[0]) <= LOCAL)
                        if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[1] - cube->indices[1]) <= LOCAL)
                           if (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[2] - cube->indices[2]) <= LOCAL) {
                              cube->localcubes[count] = cube->parent->localcubes[i]->children[cx][cy][cz];
                              count++;
                           }

   /* Interacting cubes are children of the parent's local cubes that are
      outside the LOCAL cutoff and have at least one point in them */
   
   if (cube->parent != NULL)
      for (i = 0; i < cube->parent->numlocalcubes; i++)
         for (cx = 0; cx <= 1; cx++)
            for (cy = 0; cy <= 1; cy++)
               for (cz = 0; cz <= 1; cz++)
                  if (cube->parent->localcubes[i]->children[cx][cy][cz] != NULL)
                     if ((abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[0] - cube->indices[0]) > LOCAL) ||
                         (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[1] - cube->indices[1]) > LOCAL) ||
                         (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[2] - cube->indices[2]) > LOCAL))
                        cube->numinteractingcubes++;

   /* Allocate */

   if (cube->numinteractingcubes > 0)
      cube->interactingcubes = (Cube*)calloc(cube->numinteractingcubes, sizeof(Cube*));

   count = 0;

   /* Interacting cubes are children of the parent's local cubes that are
      outside the LOCAL cutoff */
   
   if (cube->parent != NULL)
      for (i = 0; i < cube->parent->numlocalcubes; i++)
         for (cx = 0; cx <= 1; cx++)
            for (cy = 0; cy <= 1; cy++)
               for (cz = 0; cz <= 1; cz++)
                  if (cube->parent->localcubes[i]->children[cx][cy][cz] != NULL)
                     if ((abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[0] - cube->indices[0]) > LOCAL) ||
                         (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[1] - cube->indices[1]) > LOCAL) ||
                         (abs(cube->parent->localcubes[i]->children[cx][cy][cz]->indices[2] - cube->indices[2]) > LOCAL)) {
                        cube->interactingcubes[count] = cube->parent->localcubes[i]->children[cx][cy][cz];
                        count++;
                     }

   /* Recurse over children */

   for (cx = 0; cx <= 1; cx++)
      for (cy = 0; cy <= 1; cy++)
         for (cz = 0; cz <= 1; cz++)
            if (cube->children[cx][cy][cz] != NULL)
               Cube_lists(cube->children[cx][cy][cz]);
}


void Cube_memory(Cube cube, unsigned int* Cubemem, unsigned int* Umem, unsigned int* VTmem, unsigned int* UTImem, unsigned int* PVmem, unsigned int* Kmem, unsigned int* Dmem) {
   unsigned int cx, cy, cz;
   unsigned int i;
   unsigned int* gridpoints = cube->tree->gridpointsperlevel;
   unsigned int gp3 = gridpoints[cube->level]*gridpoints[cube->level]*gridpoints[cube->level];
   unsigned int padgridsize = (2*gridpoints[cube->level]-1)*(2*gridpoints[cube->level]-1)*((2*gridpoints[cube->level]-1)/2+1);
   BEMLayerType layertype = cube->tree->layertype;

   *Cubemem += sizeof(struct _Cube);
   *Cubemem += 3 * sizeof(struct _Vector3D);  // bounds, center
   *Cubemem += cube->numpanelindices * sizeof(unsigned int);  // panelindices
   *Cubemem += cube->numpointindices * sizeof(unsigned int);  // pointindices
   *Cubemem += cube->numlocalcubes * sizeof(Cube*);  // localcubes
   *Cubemem += cube->numinteractingcubes * sizeof(Cube*);  // interactingcubes

   if (!cube->leaf)
      for (cx = 0; cx <= 1; cx++)
         for (cy = 0; cy <= 1; cy++)
            for (cz = 0; cz <= 1; cz++)
               if (cube->children[cx][cy][cz] != NULL)
                  Cube_memory(cube->children[cx][cy][cz], Cubemem, Umem, VTmem, UTImem, PVmem, Kmem, Dmem);
}

void Cube_leafpanelstats(Cube cube, unsigned int* numleaves, unsigned int* maxpanels, unsigned int* numleaveswithinlimitpanels, unsigned int panellimit) {
   unsigned int cx, cy, cz;

   if (cube->leaf) {
      (*numleaves)++;
      if (cube->numpanelindices > *maxpanels)
         *maxpanels = cube->numpanelindices;
      if (cube->numpanelindices <= panellimit)
         (*numleaveswithinlimitpanels)++;
   }
   else
      for (cx = 0; cx <= 1; cx++)
         for (cy = 0; cy <= 1; cy++)
            for (cz = 0; cz <= 1; cz++)
               if (cube->children[cx][cy][cz] != NULL)
                  Cube_leafpanelstats(cube->children[cx][cy][cz], numleaves, maxpanels, numleaveswithinlimitpanels, panellimit);
}

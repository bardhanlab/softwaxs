#ifndef __SCATTERCUBE_H__
#define __SCATTERCUBE_H__

#include <assert.h>
#include "softwaxs.h"

typedef struct _scattercube {
  unsigned int level;
  unsigned int indices[3];
  Vector3D bounds[2]; // maybe not used
  real size;
  Vector3D r; // "center" of the cube
  struct _scattercube* parent;
  unsigned int leaf;
  unsigned int* panelindices;
  unsigned int numpanelindices;
  struct _scattercube* children[2][2][2];
} _scattercube;
typedef _scattercube* scattercube;
typedef enum {BULK_SOLVENT, HYDRATION_LAYER, PROTEIN_INTERIOR} scatterDensity;

unsigned int rayIntersectsTriangle(Vector3D dir, Vector3D pointStart, Panel panel, Vector3D testIntersection);
void printPanel(Panel poop);
int pointIsInsideSurface(SurfaceOperator surfoper, Vector3D point);
scatterDensity figureOutWhichRegionContainsPoint(PBEproblem problem, Vector3D point);
int recurseToGetPointsStatus(PBEproblem problem, Vector3D point);

void recursivelyInitializeCubeWithScatterData(SurfaceOperator surfoper, Cube c, real density);
void initializeOnlyOneSurfOperTree(PBEproblem problem, SurfaceOperator surfoper, real densityInside);
void initializeSurfOperatorTreesWithScatterData(PBEproblem problem, real proteinDensity, real hydrationLayerDensity);
real evaluateMolecularScatteringVolume(PBEproblem problem);
void scatterEvalInsideOutsideCubes(PBEproblem problem, Cube cube);
real scatterEvalVolume(Cube cube);
void scattercube_free(scattercube c);
scattercube scattercube_allocate(real size, Vector3D r, unsigned int level, scattercube parent);
complexreal scattercube_transform(Cube cube, Vector3D k);
complexreal evalTransformOfLeafCube(Cube c, Vector3D mu);
complexreal evalTransformCubeRecursively(Cube c, Vector3D mu);
complexreal evalTransformOneSurface(SurfaceOperator surfoper, Vector3D mu);
void evalTransform(ScatterOptions options, PBEproblem problem, ScatterParameters parameters, Vector3D mu, complexreal *excludedVolumeTerm, complexreal *hydrationLayerTerm);
void calculateScatteringAtParticularAngle(ScatterOptions options, PBEproblem problem, ScatterParameters parameters,
					  real k, real *Ik, real *IkFMS, real *debyeK,
					  real *exclK, real *borderK, real *FMS);
void calculateScatteringPattern(ScatterOptions options, PBEproblem problem, ScatterParameters parameters, ScatterIntensities intensities);//, PBEproblem problem, ScatterParameters parameters, ScatterIntensities intensities);

int recursiveCallForPointStatus(Cube c, Vector3D point);


#endif

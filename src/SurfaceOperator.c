#include "FFTSVD.h"
 
/* Constructors and Destructors */

SurfaceOperator SurfaceOperator_allocate() {
   SurfaceOperator so = (SurfaceOperator)calloc(1, sizeof(_SurfaceOperator));

   return so;
}

void SurfaceOperator_free(SurfaceOperator so) {
   unsigned int i;

   if (so->tree)
      Tree_free(so->tree);

   for (i = 0; i < so->numchildren; i++)
      SurfaceOperator_free(so->children[i]);

   free(so->children);

   if (so->charges)
      Charge_free(so->charges);

   free(so);
}

#ifndef FFTSVD_H
#define FFTSVD_H

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <sys/stat.h>
#ifndef _AIX
#include <complex.h>
#endif
#ifdef HAVE_MALLOC_H_
#include <malloc.h>
#else
#ifdef HAVE_SYS_MALLOC_H_
#include <sys/malloc.h>
#endif
#endif

/* Our AIX machine is not quite C99 compliant, this helps */

#ifdef _AIX
#define complex _Complex
#define creal __real__
#define cimag __imag__
#endif

typedef double real;
typedef double complex complexreal;
typedef double sreal;
typedef double complex complexsreal;

#define LOCAL 1  /* Only adjacent cubes are nearest neighbors, as in 
                    Greengard or Nabors, set to 2 to have next-nearest 
                    neighbors on the direct list */

typedef enum { CONSTANT_KERNEL, POISSON_KERNEL} BEMKernelType;
typedef enum { SINGLE_LAYER_INT, DOUBLE_LAYER_INT, SINGLE_AND_DOUBLE_LAYER_INT, NORMDERIV_SINGLE_LAYER_INT, NORMDERIV_DOUBLE_LAYER_INT } BEMLayerType;
typedef enum { COLLOCATION, BASIS_INNER, TEST_INNER, POINT_CHARGE, E_FIELD_TEST_ONLY} BEMDiscretizationType;

#include "Utility.h"
#include "Vector.h"
#include "Matrix.h"
#include "Vector3D.h"
#include "FlatPanel.h"
#include "Panel.h"
#include "QuadratureRule.h"
#include "Integration.h"
#include "VertFace.h"
#include "Charge.h"
#include "Cube.h"
#include "Tree.h"
#include "SurfaceOperator.h"
#include "FlatIntegration.h"

extern unsigned int resolution; // how fine an octree depth to use

#endif

#ifndef __SOFTWAXS_H__
#define __SOFTWAXS_H__

#include "FFTSVDpbeAPI.h"
#include "PBEproblem.h"

#include "scatteroptions.h"
#include "scatterparameters.h"
#include "scatterintensities.h"

#include "sphQuad.h"
#include "scattercube.h"
#include "CRYinput.h"

#define MIN_RESOLUTION 2
#define MAX_RESOLUTION 7
#define MIN_DENSITY 0.0
#define MAX_DENSITY 10.0
#define MIN_Q 0.0
#define MAX_Q 4.0
#define MIN_NUM_Q 0
#define MAX_NUM_Q 1000
#define MIN_NUM_SPH_QUAD 25
#define MAX_NUM_SPH_QUAD 2000

#endif

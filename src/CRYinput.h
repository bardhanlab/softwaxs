#ifndef __CRYINPUT_H__
#define __CRYINPUT_H__
#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#include "FFTSVD.h"
#include "PBEproblem.h"
#include "scatteroptions.h"
#include "scatterparameters.h"

#define NUM_TERMS 6
#define NUM_ATOM_TYPES 54

void evalAtomicScattering(ScatterOptions options, PBEproblem problem, ScatterParameters parameters,
			  Vector3D kVector, real atomicDensity,
			  complexreal *atomic, complexreal *FMS);

/* void GetDebyeOnly(PBEproblem problem, CRY_input cryInput, */
/* 		  real proteinDensity, unsigned int numpoints, */
/* 		  real *qList, real *IqDebyeOnly, real *IqHardSphereFMS, */
/* 		  real *IqGaussianFMS, real *IqDebyePlusHS, real *IqDebyePlusGaussian); */

#define ENERGY_THRESHOLD 2.5

#endif

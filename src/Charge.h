/* Typedef */

typedef struct _Charge {
  unsigned int numcharges;
  unsigned int globalindexstart;
  Vector3D* points;
  Vector charges;
} _Charge;

typedef _Charge* Charge;

/* Constructors and Destructors */

Charge Charge_allocate();
void Charge_free(Charge charge);

/* Operations */

void Charge_read(Charge charge, FILE* file);

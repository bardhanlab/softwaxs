/* Typedef */

typedef struct _SurfaceOperator {
   BEMKernelType kernel;
   real epsilon;
   real kappa;
   unsigned int mynumpanels;
   unsigned int startphi, startdphi;
   Tree tree;
   unsigned int numchildren;
   struct _SurfaceOperator** children;

   // added by jpb
   Vector resultInternal;
   Vector resultExternal;
   struct _SurfaceOperator* parent;

   Charge charges;
} _SurfaceOperator;

typedef _SurfaceOperator* SurfaceOperator;

/* Constructors and Destructors */

SurfaceOperator SurfaceOperator_allocate();
void SurfaceOperator_free(SurfaceOperator so);

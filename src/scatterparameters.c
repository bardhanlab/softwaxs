#include "softwaxs.h"

ScatterParameters ScatterParameters_allocate(ScatterOptions options, PBEproblem problem) {
  ScatterParameters parameters = NULL;
  parameters = (ScatterParameters)(calloc(1, sizeof(_ScatterParameters)));

  ScatterParameters_initSphQuad(parameters, options, options->numSphQuad);
  ScatterParameters_CRYload(parameters, problem, options->CRY);
  ScatterParameters_readCoeffs(parameters, options);
  parameters->useMADMAX = 0;
  
  if (options->useMADMAX) {
    printf("Error in ScatterParameters_allocate(): useMADMAX is not enabled at this time!\n");
    exit(-1);
    /* parameters->useMADMAX = 1; */
    /* ScatterParameters_anomalousInit(parameters); */
    /* ScatterParameters_anomalousLoad(parameters, options->anomalousAtomicNumber, options->anomalousFile); */
    /* ScatterParameters_anomalousGetParameters(parameters, options->anomalousAtomicNumber, options->energy, &(parameters->fprime),&(parameters->fdoubleprime)); */
  }
  
  return parameters;
}

void ScatterParameters_free(ScatterParameters parameters) {
  ScatterParameters_freeSphQuad(parameters);
  ScatterParameters_freeCRY(parameters);
  ScatterParameters_freeCoeffs(parameters);
  if (parameters->useMADMAX) {
    ScatterParameters_freeAnomalous(parameters);
  }
  free(parameters);
}


void ScatterParameters_anomalousInit(ScatterParameters parameters) {
  unsigned int i;
  parameters->fprime = 0.0;
  parameters->fdoubleprime = 0.0;
  parameters->numEnergies = (unsigned int *)calloc(NUM_ATOM_TYPES, sizeof(unsigned int));
  parameters->anomalousFprime = (real **)calloc(NUM_ATOM_TYPES, sizeof(real *));
  parameters->anomalousFdoubleprime = (real **)calloc(NUM_ATOM_TYPES, sizeof(real *));
  parameters->energies = (real **)calloc(NUM_ATOM_TYPES, sizeof(real *));
  for (i=0; i < NUM_ATOM_TYPES; i++) {
    parameters->numEnergies[i] = 0;
    parameters->anomalousFprime[i] = NULL;
    parameters->anomalousFdoubleprime[i] = NULL;
    parameters->energies[i] = NULL;
  }
}


void ScatterParameters_anomalousLoad(ScatterParameters parameters, unsigned int atomicNumber, char *filename) {
  unsigned int numlines = 0;
  unsigned int numchars = 120;
  char *curline = (char*)calloc(numchars, sizeof(char));
  FILE *INFILE = fopen(filename, "r");
  if (INFILE == NULL) {
    printf("Error in ScatterParameters_anomalousLoad: Could not open anomalous file %s.  Quitting...\n", filename);
    exit(-1);
  }
  while (!feof(INFILE)) {
    numlines++;
    fgets(curline, numchars, INFILE);
  }
  rewind(INFILE);
  
  parameters->numEnergies[atomicNumber] = numlines;
  parameters->energies[atomicNumber] = (real *)calloc(numlines, sizeof(real));
  parameters->anomalousFprime[atomicNumber] = (real *)calloc(numlines, sizeof(real));
  parameters->anomalousFdoubleprime[atomicNumber] = (real *)calloc(numlines, sizeof(real));
  unsigned int i;
  for (i = 0; i < numlines; i++) {
    fgets(curline, numchars, INFILE);
    sscanf(curline, "%lf %lf %lf", &(parameters->energies[atomicNumber][i]), &(parameters->anomalousFdoubleprime[atomicNumber][i]), &(parameters->anomalousFprime[atomicNumber][i]));
  }
  
  fclose(INFILE);
}

void ScatterParameters_anomalousGetParameters(ScatterParameters parameters, unsigned int atomicNumber, real energy, real *fprime, real *fdoubleprime) {
  if (parameters->numEnergies[atomicNumber] == 0) {
    printf("Warning:\n  There is no set of anomalous parameters for atomicNumber %d.\n  Setting fprime and fdoubleprime to zero.\n",
	   atomicNumber);
    *fprime = 0.0;
    *fdoubleprime = 0.0;
    return;
  }
  
  unsigned int i;
  for (i = 0; i < parameters->numEnergies[atomicNumber]; i++) {
    if (ENERGY_THRESHOLD > fabs(energy - parameters->energies[atomicNumber][i])) {
      *fprime = parameters->anomalousFprime[atomicNumber][i];
      *fdoubleprime = parameters->anomalousFdoubleprime[atomicNumber][i];
      return;
    }
  }
  
  printf("Error: Could not find anomalous scatterer with atomic number %d within %lf of energy %lf\n",
	 atomicNumber, (double)ENERGY_THRESHOLD, (double) energy);
  exit(-1);
}

void ScatterParameters_freeAnomalous(ScatterParameters parameters) {
  unsigned int i;
  for (i = 0; i < NUM_ATOM_TYPES; i++) {
    if (parameters->anomalousFprime[i]) 
      free(parameters->anomalousFprime[i]);
    if (parameters->anomalousFdoubleprime[i]) 
      free(parameters->anomalousFdoubleprime[i]);
    if (parameters->energies[i])
      free(parameters->energies[i]);
  }
  free(parameters->energies);
  free(parameters->anomalousFprime);
  free(parameters->anomalousFdoubleprime);
  free(parameters->numEnergies);
}

void ScatterParameters_initSphQuad(ScatterParameters parameters, ScatterOptions options, unsigned int d) {
  parameters->numSphQuadPoints = d;
  parameters->sphQuadPoints = (Vector3D *)calloc(d, sizeof(Vector3D));
  parameters->sphQuadWeights = (real *)calloc(d, sizeof(real));

  unsigned int i;
  char filename[100];
  sprintf(filename,"%s/sphQuad_%d.txt",options->parameterDirectory,d);
  FILE *infile = fopen(filename, "r");
  if (infile == NULL) {
	 printf("Error in initQuad(): could not open %d-point quadrature rule.\n", d);
	 exit(-1);
  }
  for (i = 0; i < parameters->numSphQuadPoints; i++) {
	 parameters->sphQuadPoints[i] = Vector3D_allocate();
	 fscanf(infile, "%lf %lf %lf %lf", &(parameters->sphQuadPoints[i]->x), &(parameters->sphQuadPoints[i]->y),
			  &(parameters->sphQuadPoints[i]->z), &(parameters->sphQuadWeights[i]));
	 parameters->sphQuadWeights[i] = parameters->sphQuadWeights[i] / (4.0 * M_PI);
  }
  fclose(infile);
  
}


void ScatterParameters_freeSphQuad(ScatterParameters parameters) {
  unsigned int i;
  
  for (i = 0; i < parameters->numSphQuadPoints; i++) {
    Vector3D_free(parameters->sphQuadPoints[i]);
  }
  free(parameters->sphQuadPoints);
  free(parameters->sphQuadWeights);
}

void ScatterParameters_quadCartToSph2(real* rho, real* theta, real* phi, Vector3D pnt) {
  *rho = Vector3D_length(pnt);
  *theta = atan2(pnt->y, pnt->x);
  *phi = 0;
  if (((*rho) * (*rho)) > 0.0)
    *phi = acos(pnt->z/ *rho);
}

void ScatterParameters_quadSphToCart2(Vector3D pnt, real rho, real theta, real phi) {
  pnt->x = rho * cos(theta) * sin(phi);
  pnt->y = rho * sin(theta) * sin(phi);
  pnt->z = rho * cos(phi);
}


void ScatterParameters_getAtomType(char *type, unsigned int *atomicNumberMinus1, real *atomHardSphereRadius, real *atomGaussianRadius, real *atomMass, real *atomVolume) {
  if (!strcmp("H",type)) {
    *atomicNumberMinus1 = 1 - 1;
    *atomGaussianRadius = (real)pow((double)5.15,(double)1./3.); //5.15//7.24
    *atomHardSphereRadius = 1.0;
    *atomMass   = 1.008; //first number in comments above is FMS; second is van der Waals from Traube (see FMS)
    *atomVolume = 5.15;
  } else if (!strcmp("He",type)) {
    *atomicNumberMinus1 = 2 - 1;
    printf("Error in ScatterParameters_getAtomType: Atom He is not defined in SoftWAXS yet.\n");
    exit(-1);
  } else if (!strcmp("C", type)) {
    *atomicNumberMinus1   = 6 - 1;
    *atomGaussianRadius   = (real)pow((double)16.44,(double)1./3.); //16.44//20.58
    *atomHardSphereRadius = 2.2; // (real)pow((double)16.44,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.);
    *atomVolume           = 16.44;
    *atomMass             = 12.011;
  } else if (!strcmp("N",type)) {
    *atomicNumberMinus1 = 7 - 1;
    *atomGaussianRadius   = (real)pow((double)2.49,(double)1./3.);
    *atomHardSphereRadius = 0.7; //(real)pow((double)2.49,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.); 
    *atomVolume           = 2.49;
    *atomMass             = 14.01;
  } else if (!strcmp("O", type)) {
    *atomicNumberMinus1 = 8 - 1;
    *atomGaussianRadius = (real)pow((double)9.13,(double)1./3.); //9.13//14.71
    *atomHardSphereRadius = 1.2; // (real)pow((double)9.13,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.);  //WRONG
    *atomVolume          = 9.13;
    *atomMass            = 16.00;
  } else if (!strcmp("S", type)) {
    *atomicNumberMinus1   = 16 - 1;
    *atomGaussianRadius   = (real)pow((double)19.86,(double)1./3.);
    *atomHardSphereRadius = 1.2; // (real)pow((double)19.86,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.);
    *atomVolume           = 19.86;
    *atomMass             = 32.07;
  } else if (!strcmp("FE", type)) {
    *atomicNumberMinus1   = 26 - 1;
    *atomGaussianRadius   = (real)pow((double)7.99,(double)1./3.);
    *atomHardSphereRadius = (real)pow((double)7.99,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.);
    *atomMass   = 55.845;  // wrong
    *atomVolume = 7.99; // wrong
  } else if (!strcmp("SE", type)) { 
    *atomicNumberMinus1 = 34 - 1;
    *atomGaussianRadius = (real)pow((double)7.99,(double)1./3.); // wrong
    *atomHardSphereRadius = (real)pow((double)7.99,(double)1./3.) / pow(4.0 * M_PI/3.0,(double)1./3.);   // wrong
    *atomMass   = 55.845; // wrong
    *atomVolume = 7.99; // wrong
    } else {
    printf("Warning in ScatterParameters_getAtomType: did not recognize %s.\n", type);
    *atomicNumberMinus1 = 1024;
    *atomGaussianRadius = 0.0;
    *atomHardSphereRadius = 0.0;
    *atomMass = 0.0;
    *atomVolume = 0.0;
  }
  //  *atomGaussianRadius[i] = (real) pow(myFourThirdsPi * pow((*atomHardSphereRadius), 3.0), 1./3.);

}

void ScatterParameters_CRYload(ScatterParameters parameters, PBEproblem problem, char *filename) {
  FILE *CRYin = fopen(filename, "r");
  if (CRYin == NULL) {
    printf("Error in ScatterParameters_CRYload: Could not open CRY file %s for reading.\n", filename);
    exit(-1);
  }

  fscanf(CRYin, "%d", &(parameters->numAtoms));
  printf("numatoms = %d\n",parameters->numAtoms);
  assert(problem->numpdbentries == parameters->numAtoms);

  parameters->atomTypes = (unsigned int *)calloc(parameters->numAtoms, sizeof(unsigned int));
  parameters->atomLocations = (Vector3D *)calloc(parameters->numAtoms, sizeof(Vector3D));
  parameters->atomTemperatureFactors = (real *)calloc(parameters->numAtoms, sizeof(real));
  parameters->atomHardSphereRadii = (real *)calloc(parameters->numAtoms, sizeof(real));
  parameters->atomGaussianRadii = (real *)calloc(parameters->numAtoms, sizeof(real));
  parameters->atomMass = (real *)calloc(parameters->numAtoms, sizeof(real));
  parameters->atomVolume = (real *)calloc(parameters->numAtoms, sizeof(real));
  real myFourThirdsPi = 4.0 * M_PI / 3.0;
  unsigned int i;
  char newtype[10];
  real newx, newy, newz;
  real newTemp;
  real totalE = 0.0;
  real molarMass = 0.0;
  real totalX = 0.0;
  real totalY = 0.0;
  real totalZ = 0.0;
  real totalAtomicVolume = 0.0;
  real aveAtomicVolume = 0.0;
  for (i = 0; i < parameters->numAtoms; i++) {
    parameters->atomLocations[i] = Vector3D_allocate();
    fscanf(CRYin, "%s %lf %lf %lf %lf", newtype, &(parameters->atomLocations[i]->x),  &(parameters->atomLocations[i]->y),
	   &(parameters->atomLocations[i]->z),  &(parameters->atomTemperatureFactors[i]));
    ScatterParameters_getAtomType(newtype, &(parameters->atomTypes[i]), &(parameters->atomHardSphereRadii[i]), &(parameters->atomGaussianRadii[i]),
				  &(parameters->atomMass[i]), &(parameters->atomVolume[i]));
    totalX += parameters->atomLocations[i]->x;
    totalY += parameters->atomLocations[i]->y;
    totalZ += parameters->atomLocations[i]->z;
    totalE += parameters->atomTypes[i] + 1;
    totalAtomicVolume += parameters->atomVolume[i];
    molarMass += parameters->atomMass[i];
  }

  aveAtomicVolume = totalAtomicVolume / parameters->numAtoms;
  parameters->aveAtomicRadius = pow(aveAtomicVolume/(4.0*M_PI/3.0),1./3.);
  Vector3D center = Vector3D_allocate();
  center->x = totalX/parameters->numAtoms;
  center->y = totalY/parameters->numAtoms;
  center->z = totalZ/parameters->numAtoms;
  real totalSquaredDistanceElec = 0.0;
  for (i = 0; i < parameters->numAtoms; i++) {
	 totalSquaredDistanceElec += (parameters->atomTypes[i]+1) * Vector3D_distance(center, parameters->atomLocations[i]) *
		Vector3D_distance(center, parameters->atomLocations[i]);
  } 
  Vector3D_free(center);

  printf("Average atomic radius: %lf\n", parameters->aveAtomicRadius);
  printf("Total number of electrons should be %f\n",totalE);
  printf("Geometric center = %lf   %lf   %lf\n", totalX/parameters->numAtoms, totalY/parameters->numAtoms, totalZ/parameters->numAtoms);
  printf("Electron Rg = %lf\n",sqrt(totalSquaredDistanceElec/totalE));
  printf("Molar mass should be %f\n", molarMass);
}

void ScatterParameters_freeCRY(ScatterParameters parameters) {
  unsigned int i;

  free(parameters->atomTypes);
  free(parameters->atomMass);
  free(parameters->atomVolume);
  free(parameters->atomTemperatureFactors);
  free(parameters->atomHardSphereRadii);
  free(parameters->atomGaussianRadii);
  for (i = 0; i < parameters->numAtoms; i++) {
    Vector3D_free(parameters->atomLocations[i]);
  }
  free(parameters->atomLocations);
}


void ScatterParameters_readCoeffs(ScatterParameters parameters, ScatterOptions options) {
  FILE *A, *B;
  unsigned int i;

  parameters->lowA  = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  parameters->medA  = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  parameters->highA = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  parameters->lowB  = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  parameters->medB  = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  parameters->highB = (real *)malloc(NUM_TERMS * NUM_ATOM_TYPES * sizeof(real));
  
  A = fopen(options->lowAfile,"r");
  B = fopen(options->lowBfile,"r");
  if (A == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open lowA file %s.\n", options->lowAfile);
    exit(-1);
  }
  if (B == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open lowB file %s.\n", options->lowBfile);
    exit(-1);
  }
  
  for (i = 0; i < NUM_TERMS * NUM_ATOM_TYPES; i++) {
	 fscanf(A, "%lf", &(parameters->lowA[i]));
	 fscanf(B, "%lf", &(parameters->lowB[i]));
  }
  fclose(A);
  fclose(B);

  A = fopen(options->medAfile,"r");
  B = fopen(options->medBfile,"r");
  if (A == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open medA file %s.\n", options->medAfile);
    exit(-1);
  }
  if (B == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open medB file %s.\n", options->medBfile);
    exit(-1);
  }
  
  for (i = 0; i < NUM_TERMS * NUM_ATOM_TYPES; i++) {
	 fscanf(A, "%lf", &(parameters->medA[i]));
	 fscanf(B, "%lf", &(parameters->medB[i]));
  }
  fclose(A);
  fclose(B);

  A = fopen(options->highAfile,"r");
  B = fopen(options->highBfile,"r");
  if (A == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open highA file %s.\n", options->highAfile);
    exit(-1);
  }
  if (B == NULL) {
    printf("Error in ScatterParameters_readCoeffs: could not open highB file %s.\n", options->highBfile);
    exit(-1);
  }
  
  for (i = 0; i < NUM_TERMS * NUM_ATOM_TYPES; i++) {
    fscanf(A, "%lf", &(parameters->highA[i]));
    fscanf(B, "%lf", &(parameters->highB[i]));
  }
  fclose(A);
  fclose(B);

}

void ScatterParameters_freeCoeffs(ScatterParameters parameters) {
  free(parameters->lowA);
  free(parameters->medA);
  free(parameters->highA);
  free(parameters->lowB);
  free(parameters->medB);
  free(parameters->highB);
}

/* #ifdef DEBUG_CRY */
/*   unsigned int j; */
/*   printf("***********  lowA:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",lowA[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */

/*   printf("***********  medA:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",medA[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */

/*   printf("***********  highA:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",highA[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */

/*   printf("***********  lowB:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",lowB[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */

/*   printf("***********  medB:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",medB[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */

/*   printf("***********  highB:\n"); */
/*   for (i = 0; i < NUM_ATOM_TYPES; i++) { */
/* 	 for (j = 0; j < NUM_TERMS; j++) { */
/* 		printf("%f  ",highB[NUM_TERMS * i + j]); */
/* 	 } */
/* 	 printf("\n"); */
/*   } */
/* #endif */

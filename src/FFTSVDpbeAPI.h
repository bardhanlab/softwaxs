#ifndef FFTSVDpbeAPI_H
#define FFTSVDpbeAPI_H

#include "FFTSVD.h"
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <stdarg.h>
#include <errno.h>

#define GRID_SIZE 4
#define SVD_ERROR 1e-5

#define MAX_PANELS_PER_FINEST_CUBE 32

/* Important Structures */

typedef struct {
   char record[7];
   unsigned int atomnumber;
   char atomname[5];
   char alternatelocation;
   char residuename[4];
   char chain;
   unsigned int residuenumber;
   char residueinsertion;
   real x;
   real y;
   real z;
   real occupancy;
   real temperature;
   unsigned int footnotenumber;

   real radius;
   real charge;
   real potential;
} PDBentry;

typedef struct {
   char atomlabel[7];
   char residuelabel[4];
   real radius;
} SIZentry;

/* Parameters */

extern real dielectricvertexdensity;
extern real saltvertexdensity;
extern real innerdielectric;
extern real outerdielectric;
extern real ionicstrength;
extern real ionexclusionradius;
extern real proberadius;

/* Utility */

void error(char* message, ...);
void warning(char* message, ...);
void removeWhitespace(char* s);
int yesno(char* s);

/* Input */

void readParams(const char* filename);
void checkParams();
void readPDB(const char* filename, unsigned int* numPDBentries, PDBentry** PDBentries);
void readCRD(const char* filename, unsigned int* numPDBentries, PDBentry** PDBentries);
void readXYZR(const char* filename, unsigned int* numPDBentries, PDBentry** PDBentries);
void readSIZ(const char* filename, unsigned int* numSIZentries, SIZentry** SIZentries);
void assignRadiiCharges(PDBentry* PDBentries, unsigned int numPDBentries, SIZentry* SIZentries, unsigned int numSIZentries);

/* Molecule Handling */

void generateFlatSRF(const char* pdbfilename, const char* sizfilename, const char* srffilename);
void readSRF(const char* filename, Panel*** saltpanels, unsigned int** numsaltpanels, unsigned int* numsalts,
Panel*** dielectricpanels, unsigned int** numdielectricpanels, unsigned int* numdielectrics, unsigned int** dielectricparent,
Panel*** dielectriccavitypanels, unsigned int** numdielectriccavitypanels, unsigned int* numdielectriccavities, unsigned int** dielectriccavityparent,
Panel*** saltcavitypanels, unsigned int** numsaltcavitypanels, unsigned int* numsaltcavities, unsigned int** saltcavityparent, unsigned int* numtotalpanels);

/* Surface Operator */

void generateSurfaceOperator(unsigned int resolution, SurfaceOperator* pbesurfaceoperator,
PDBentry* PDBentries, unsigned int numPDBentries,
Panel** saltpanels, unsigned int* numsaltpanels, unsigned int numsalts, 
Panel** dielectricpanels, unsigned int* numdielectricpanels, unsigned int numdielectrics, unsigned int* dielectricparent,
Panel** dielectriccavitypanels, unsigned int* numdielectriccavitypanels, unsigned int numdielectriccavities, unsigned int* dielectriccavityparent, 
Panel** saltcavitypanels, unsigned int* numsaltcavitypanels, unsigned int numsaltcavities, unsigned int* saltcavityparent,
unsigned int numtotalpanels);

#endif

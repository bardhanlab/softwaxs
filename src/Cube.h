/* Typedef */

typedef struct _Cube {
  unsigned int level;  /* depth of this cube in the tree */
  unsigned int indices[3];  /* grid coordinates of this cube on this level */
  Vector3D bounds[2];  /* diagonal corners of the cube */
  Vector3D center; /* center of the cube */
  struct _Cube* parent;  /* Parent cube */
  unsigned int leaf;  /* 1 if a leaf, otherwise 0 */
  unsigned int* panelindices;  /* indices of the panels contained in this cube */
  unsigned int numpanelindices;  /* number of panels in this cube */
  unsigned int* pointindices;  /* indices of the collocation points contained in this cube */
  unsigned int numpointindices;  /* number of collocation points in this cube */
  struct _Cube** localcubes;  /* pointers to local cubes */
  unsigned int numlocalcubes;   /* number of local cubes */
  struct _Cube** interactingcubes;  /* pointers to interacting cubes */
  unsigned int numinteractingcubes;  /* number of interacting cubes */
  struct _Cube* children[2][2][2];   /* pointers to children */
  unsigned int drows, dcolumns;   /* dimensions for D if a leaf (numpanelindices + numpanelindices in local cubes) */
  unsigned int rowrank, columnrank;   /* inner dimensions for VTsrc and Udest */
  struct _Tree* tree; /* the tree this cube belongs to */
  unsigned int isInsideProtein;
  real densityInsideCube;
} _Cube;

typedef _Cube* Cube;

/* Constructors and Destructors */

Cube Cube_allocate(Panel* panels, unsigned int* panelindices, unsigned int numpanelindices, Vector3D* points, unsigned int* pointindices, unsigned int numpointindices, unsigned int level, unsigned int indices[3], Vector3D bounds[2], Cube parent, struct _Tree* tree, unsigned int partitioningdepth);
void Cube_free(Cube cube);

/* Operations */

void Cube_lists(Cube cube);
void Cube_memory(Cube cube, unsigned int* Cubemem, unsigned int* Umem, unsigned int* VTmem, unsigned int* UTImem, unsigned int* PVmem, unsigned int* Kmem, unsigned int* Dmem);
void Cube_leafpanelstats(Cube cube, unsigned int* numleaves, unsigned int* maxpanels, unsigned int* numleaveswithinlimitpanels, unsigned int panellimit);

extern unsigned int trackCube;


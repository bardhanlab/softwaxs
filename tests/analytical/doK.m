function K = doK(s, r1, r2)

if min(s < 1e-6)
  fprintf('Error: all elements of s must be > 1e-6!\n');
  K = 0; 
  return;
end

Kr1 = (-r1 * 2 * pi * s .*cos(r1*2*pi*s) + sin(r1 *2*pi*s))./((2*pi*s).^2);
Kr2 = (-r2 * 2 * pi * s .*cos(r2*2*pi*s) + sin(r2 *2*pi*s))./((2*pi*s).^2);
K = 2 * (Kr2 - Kr1) ./ s;
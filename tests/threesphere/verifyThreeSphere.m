addpath('../analytical');
s = 0:0.002:2;
s = s';
K3 = doK(s/2/pi, 0, 3);
K5 = doK(s/2/pi, 0, 5);
K7 = doK(s/2/pi, 0, 7);

allK = [K3 K5 K7];

R = [ 0  13 8; 13 0 15.2643; 8 15.2643 0];

I = zeros(length(s),1);
for iterFoo=1:length(s)
  for i = 1:3
	 f_i = allK(iterFoo, i);
	 for j = 1:3
		f_j = allK(iterFoo, j);

		if i==j
		  I(iterFoo) = I(iterFoo) + f_i*f_j;
		else
		  r_ij = R(i,j);
		  q = s(iterFoo);
		  I(iterFoo) = I(iterFoo) + f_i*f_j*sin(q*r_ij)/(q * r_ij);
		end
	 end
  end
end
  
ANAL = semilogy(s,I);
set(ANAL, 'LineWidth', 3);
hold on;
GCA = gca;
set(GCA, 'FontSize', 16);
xlabel('q (Angstroms^{-1})')
ylabel('I(q) ')

return

j4_1 = load('jsr_08_depth4_nq400.dat');
j4_2 = load('jsr_08_depth4_nq900.dat');
j5_2 = load('jsr_08_depth5_nq900.dat');
j5_1 = load('jsr_08_depth5_nq400.dat');
j6_2 = load('jsr_08_depth6_nq900.dat');

J41 = semilogy(j4_1(:,1), j4_1(:,4), 'g');
J42 = semilogy(j4_2(:,1), j4_2(:,4), 'r');
J51 = semilogy(j5_1(:,1), j5_1(:,4), 'm');
J52 = semilogy(j5_2(:,1), j5_2(:,4), 'k');
%J62 = semilogy(j6_2(:,1), j6_2(:,2)/.334/.334, 'c');

subIndex1 = 1:20:max(size((j4_1)));
subIndex2 = 10:20:max(size((j4_1)));

%J41_sub = semilogy(j4_1(subIndex1,1), j4_1(subIndex1,2), 'gs');
%J42_sub = semilogy(j4_2(subIndex1,1), j4_2(subIndex1,2), 'r^');
%J51_sub = semilogy(j5_1(subIndex2,1), j5_1(subIndex2,2), 'md');
%J52_sub = semilogy(j5_2(subIndex2,1), j5_2(subIndex2,2), 'ko');

%set(J41_sub,'MarkerSize',6);
%set(J42_sub,'MarkerSize',12);
%set(J51_sub,'MarkerSize',6);
%set(J52_sub,'MarkerSize',12);

set(J41,'LineWidth', 1.5);
set(J42,'LineWidth', 1.5);
set(J51,'LineWidth', 1.5);
set(J52,'LineWidth', 1.5);
%set(J62,'LineWidth', 2);
%set(J41_sub,'LineWidth', 1.5);
%set(J42_sub,'LineWidth', 1.5);
%set(J51_sub,'LineWidth', 1.5);
%set(J52_sub,'LineWidth', 1.5);

legend('Analytical', 'Depth=4,N=400','Depth=4,N=900', ...
		 'Depth=5,N=400','Depth=5,N=900')

print -depsc2 three-spheres-excl-only.eps
print -dpng three-spheres-excl-only.png  

2/7/17:

Building should now be as simple as running three commands:
./autogen.sh
./configure
make

To test it, change directories to softwaxs/tests/lysozyme

and run

./../src/doScatter --param foo.param --siz foo.siz --pdb 6LYZ.hbuild.pdb --output newoutput.txt --cry 6LYZ.hbuild.cry --config ../sphere5/swcfg --prmdir ../../Parameters/ --srf 6LYZ.hbuild.srf

Note that you can't run it on your proteins until you set up and run
MSMS to generate a .srf file on your own, and create a .CRY file from
the .PDB file for your protein.  For the latter, run

softwaxs/util/pdb_cry.pl myprotein.pdb myprotein.cry

generating SRF files are a little more complicated: call it with something of the form

softwaxs/src/meshmaker myprotein.pdb softwaxs/Parameters/radii.siz mysurfacefile.srf 1.4 2.0 2 2 1 1 ./

the first argument is the PDB file of interest.  the second is the
radii.siz file, which tells meshmaker how big each atom is.  you may
find that some atoms in your PDB file do not have assigned radii.  If
so, please let me know.  the third argument is the output SRF
filename. the fourth argument (1.4) is the size of the water probe
sphere, 1.4 Angstroms usually.  the fifth argument (here 2.0) is the
width of the hydration shell.  you may need to vary this.  the next 4
arguments are the level of discretization (how many vertices, and thus
triangles, you will have per square Angstrom of surface). "2 2 1 1"
should work but you may want to increase the first two numbers and use
"4 4 1 1" (the two 1s should remain, because other values are not
defined in this code).  Last, you can use a relative path name to
store the triangle meshes (.vert and .face) files that are outputs
from the binary MSMS, which meshmaker calls.  Note that if you move
the .srf file, you also need to move the .vert and .face files
accordingly.


pdb_cry.pl will issue warnings if it doesn't recognize specific atom
names.  this happens often if the PDB file has extraneous molecules
(co-solvents) or ligands, or if you have added blocking groups to the
terminal, (e.g. ACE or CT3 patches, in CHARMM).  the default rule is
to assume that the atom is the type of the first character, e.g. atom
named "SG" in cysteine is assumed to be "S" (sulfur).  If you find other
behavior or want to add names, please let me know.

in order for meshmaker to work, you need to install an MSMS binary and
make it known through the MSMS_PATH environment variable.  on my mac, for example,
I put the binary in /Users/jbardhan/bin, so I run
export MSMS_PATH=/Users/jbardhan/bin/msms

If meshmaker gives you an error like

Command is (null) -no_header -probe_radius 1.400000 -density 2.000000
-if /tmp/xyzrwhJKHt -of ./surfQWfAVw > /tmp/outxejTR6

The (null) indicates that it isn't getting the MSMS_PATH.

Notes to myself: 

1) I don't think --param, --siz, or --config are actually used anymore
(they're dummy arguments at this point) so it would be good to check,
and remove them if so.  --config may be useful for other things in the
future.




9/25/13:

Building should now be as simple as running three commands:
./autogen.sh
./configure
make

However, it won't do much useful yet, unless you really know
how to set up and run MSMS.

9/24/13:

A "to do" list:

1. command line argument handling via getopt.  including defaults.

2. documentation via doxygen

3. suite of test cases and automated verification.

4. better handling of environment variables

5. add cavity / no cavity choice to command line, default no cavity.

6. tests for B factor usage

7. tests for anomalous scattering

8. remove SIZ and CRG entirely

9. replace initQuad with the method used by S. Park

10. why do we get small changes << 1% for threespheres example, depth
5vs6 at 900pts... why are our depth 5 results so much closer to theirs
at depth6 than the earlier depth 5?

12. why do we no longer delete a few panels, outer stern for threespheres?

13. make anomalous a command line option

14. add qMin, qMax, numQ to input options rather than hard coded

15. option to use NULL atom instead of hydrogen for unidentified atom types in .CRY file

16. make numSphQuadPoints a command line option

17. make parameter file more useful by allowing all command line
options to be set in there.  then default gets overwritten by
parameter file, which gets overwritten by command line options.

18. in scatteroptions_parseArgs, instead of strdup we should have a
smarter function that prepends parameter dir IF (a) paramdir is
specified and (b) the given filename does not have a '/'

19. scatteroptions config file should have some kind of path
associated with it

20. the atomic form factors need to be checked carefully!

21. Export MSMS_PATH to the location of the msms binary.  (Our binary
"meshmaker" calls it.)

9/24/13: None of the original instructions (below) work anymore..  I've been
rebuilding SoftWAXS by merging the three codebases and eliminating
unneeded components.  For right now, the best you can do is
successfully build SoftWAXS by calling "make" in the source directory.
I'm going as fast as I can to make this usable.

Ignore all of the below---it's just here to illustrate that SoftWAXS
used to be very complicated!
---------------------------------------------------------------------

How to build and run SoftWAXS (Bardhan, Park, Makowski 2009):

0. pre-installation libraries and auxiliary tools
a.) install or build FFTW3, from
http://www.fftw.org/

b.) install msms, available at
http://www.scripps.edu/~sanner/html/msms_home.html

1. there are three directories in which you need to build relevant
object files: "FFTSVDcp" "hipd3" and "scatter."  We address each in turn.

in FFTSVDcp/

2. if you want to be able to have solvent-filled cavities add
 "-all_components" to the msms command (line 133).  This is sort of a
 problematic question because some proteins have LOTS of tiny cavities
 and I have not yet thoroughly tested how strongly they impact the
 resulting SAXS/WAXS patterns.  the results in our paper are run with
 the code -as is-, that is, without "all_components".  (see the msms
 documentation for some more details).
 
3. make meshmaker2 (set HOST in the Makefile to the appropriate
setting--see the files make-*.inc)

4. to run meshmaker2 you need the files:

a.) protein.pdb (you will need to add hydrogens and missing atoms using a
program of your choice--CHARMM, NAMD/VMD, AMBER, Chimera, etc; if you
run SoftWAXS on a PDB file straight from the data bank, you might get
results that you don't want, and none of the output will warn you of
that).

b.) radii.siz (provided). of course these are just a list of
parameters, and in our paper we explored a big chunk of reasonable
parameter space.

5. then you can actually mesh your protein.  run 

meshmaker2 protein.pdb radii.siz output.srf 1.4 3.0 2.0 2.0 1 1 .

the explanation of this line:

a.) the 1.4 is the probe radius in Angstroms (usually taken to be 1.4 A,
similar to the radius of the water oxygen).

b.) the 3.0 is the width of the hydration shell, again in Angstroms (you
can adjust this as we did in the paper, same as for the probe radius).

c.) the two "2.0" denote the vertex densities on the protein and hydration
surfaces that will be used in triangulation (vertices per square
Angstrom).   beyond 2.0 not much improvement is seen.

d.) the two "1" tell meshmaker2 to use flat triangles.  (in our work on
simulating protein electrostatics we have also employed curved
triangles, but nobody in WAXS should bother).

e.) the period at the end is a reference to a directory--you can direct
the program to put the vertices and polygon files in another directory
if you so choose, so as not to clutter your "work" directory.  the
.srf file will then point to the directory in which you put the
geometry data.  if you're not generating the cavities (ie, if you did
not add "all_components"), it is likely not worth your time to fuss around
with this, so just add the period to say "put the geometry files in the
current directory."

in hipd3/
6. type "make obj".  (this provides a somewhat
cleaner interface to the geometry, and is designed originally for
optimizing electrostatic interactions between molecules--see Bardhan,
Altman et al, J. Chem. Theory Comput. 2009).  You will also need to
set the appropriate HOST type here as before.

in scatter/

7. after setting HOST in Makefile, make doScatter (this is the
executable for SoftWAXS).

8. generate a .cry file from the pdb file of interest using pdb_cry.pl
(provided).  pdb_cry.pl is a simpler version of a script i have used
that employs code I don't have permission to distribute, so _please_
do not hesitate to contact me if it has bugs.  essentially it's a very
simple script that just translates the atom -names- in the PDB (CA,
OG, etc) to the elements themselves (thus: C, O, etc.).

9. doScatter needs the following files

a.) parameterFile (provided as standard.prm)
b.) radii.siz (same as for meshmaker2)
c.) protein.pdb
d.) output.srf
e.) protein.cry 

10. you will also need to set the environment variable MM_PARAMETER_DIR to
wherever you are keeping the numerical quadrature files that are
provided.  (Sanghyun Park has nice code that will someday make it
unnecessary to keep these around...)

11. call doScatter along the lines of :

doScatter standard.prm radii.siz protein.pdb protein_mesh.srf protein.cry 6 0.334 0.003 7110.0 0 1.0 output.dat

a.) resolution (6): an integer that tells the program how deep an
octree to form.  you will want to verify that the resolution you
provide is adequate, but you can estimate by contemplating the maximum
q you wish to examine, and ensuring that the smallest cube size will
be much smaller than 1/q_max. (hopefully in a future version this
parameter will be picked intelligently by the software using this rule
automatically, and only overridden at the user's request, but we're
not there yet.)  normally 6 works fine for small proteins like
myoglobin but you need 7 for hemoglobin.  (see the paper for an
example of inaccuracies that can result from using too few levels in
the octree).

b.) protDensity: normally this will be 0.334 (electron density in the
solvent, in electrons per cubic Angstrom)

c.) hydDensity: often this will be 0.003 (in keeping with CRYSOL), the
change in solvation shell electron density relative to bulk solvent.

d.) incidentEnergy: the incident photon energy in eV.  not useful
unless you are doing anomalous SAXS, and the code's use for this is
unsupported at this time.  Please let me know if you'd like to use
this capability!

e.) useBeta?  at one point the code would incoporate the B-factors of
atoms (see the paper).  no longer used, but if you're interested
please let me know.  so for now, just say "0".

f.) Rm_scale_factor. can be used to scale all the atoms' radii to match
the total excluded volume calculated by Fraser-MacRae-Suzuki model (see
the discussion in the original CRYSOL paper). unless you really want to
play with these methods, it's okay to just leave it at "1.0".

g.) output.dat is the name of the output data filename.  the columns
of this file are, in order:

* q (Angstroms^-1)
* total scattering using the octree volume method
* atomic form factor contribution to scattering
* protein excluded volume contribution to scattering
* hydration shell contribution to scattering

* total scattering using the Fraser-MacRae-Suzuki method (see note below)
* the Fraser-MacRae-Suzuki excluded volume scattering

* atomic form factor scattering (computed using Debye formula)
* hard sphere excluded volume scattering (computed using Debye)
* Gaussian excluded volume scattering (computed using Debye)
* atomic form factor + hard sphere excl. vol. scattering (by Debye formula)
* atomic form factor + Gaussian excl. vol. scattering (by Debye formula)

The first five columns are the same order as one obtains using CRYSOL.
Columns 6 and 7, which use Fraser-MacRae-Suzuki Gaussians to represent
excluded volume as a sum of atomic volume functions, still use the
numerical integration over the sphere as detailed in the paper.  The
final five columns employ the Debye formula, and thereby provide a
means to check that the numerical integration is being done properly.
However, the pairwise Debye formula can be very slow for large
molecules and therefore in general it is recommended that line 152
(calling "GetDebyeOnly") be commented out for larger systems.

12. for the moment the q range and number of q's used are hardcoded in
doScatter.c.  obviously that will change going forward and become part
of the command line options.
